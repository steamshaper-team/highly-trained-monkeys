/*
 * © Iad Srl - 2014
 * All Rights Reserved.
 * 
 */
package org.steamshaper.htm.service.interfaces;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * The Class ServiceResponse.
 * 
 * @param <PAYLOAD>
 *            the generic type
 */
public class ServiceResponse<PAYLOAD> {

	/** The payload. */
	PAYLOAD payload;

	/** The exit status. */
	ExitStauts exitStatus = null;

	/** The errors message. */
	private List<String> errorsMessage = null;

	/**
	 * The Enum ExitStauts.
	 */
	public enum ExitStauts {

		/** The ok. */
		OK,
		/** The error. */
		ERROR,
		/** The warning. */
		WARNING
	}

	/**
	 * Instantiates a new service response.
	 * 
	 * @param payload
	 *            the payload
	 */
	public ServiceResponse(PAYLOAD payload) {
		this.payload = payload;
	}

	/**
	 * Instantiates a new service response.
	 */
	public ServiceResponse() {

	}

	/**
	 * Gets the payload.
	 * 
	 * @return the payload
	 */
	public PAYLOAD getPayload() {
		return payload;
	}

	/**
	 * Sets the payload.
	 * 
	 * @param payload
	 *            the new payload
	 */
	public void setPayload(PAYLOAD payload) {
		this.payload = payload;
	}

	/**
	 * Adds the error message.
	 * 
	 * @param error
	 *            the error
	 */
	public void addErrorMessage(String error) {
		if (errorsMessage == null) {
			errorsMessage = new ArrayList<String>();
		}
		errorsMessage.add(error == null ? "UNKNOWN ERROR" : error);
	}

	/**
	 * Checks for errors.
	 * 
	 * @return true, if successful
	 */
	public boolean hasErrors() {
		return errorsMessage != null;
	}

	/**
	 * Gets the errors.
	 * 
	 * @return the errors
	 */
	public List<String> getErrors() {
		if (errorsMessage != null) {
			return Collections.unmodifiableList(errorsMessage);
		}
		return Collections.emptyList();
	}

}
