package org.steamshaper.htm.service.interfaces;

public class ServiceRequest<PAYLOAD> {
	
	PAYLOAD payload;
	
	public ServiceRequest(PAYLOAD payload){
		this.payload = payload;
	}

	/**
	 * @return the payload
	 */
	public PAYLOAD getPayload() {
		return payload;
	}
	
	public boolean hasAPayload(){
		return this.payload!=null;
	}


}
