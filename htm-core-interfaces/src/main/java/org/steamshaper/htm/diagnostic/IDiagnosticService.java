package org.steamshaper.htm.diagnostic;

import java.util.List;

import org.steamshaper.htm.diagnostic.IListableProbe;
import org.steamshaper.htm.service.interfaces.ServiceRequest;
import org.steamshaper.htm.service.interfaces.ServiceResponse;

public interface IDiagnosticService {
	
	ServiceResponse<List<IListableProbe>> getAllProbe(ServiceRequest<Void> request);
	
	
}
