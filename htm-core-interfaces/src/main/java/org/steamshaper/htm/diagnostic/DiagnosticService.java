package org.steamshaper.htm.diagnostic;

import java.util.ArrayList;
import java.util.List;

import org.steamshaper.htm.Help;
import org.steamshaper.htm.service.interfaces.ServiceRequest;
import org.steamshaper.htm.service.interfaces.ServiceResponse;

public class DiagnosticService implements IDiagnosticService {

	private IDiagnostic diagnostigMgr = Help.withDiagnostic.getManager();

	@Override
	public ServiceResponse<List<IListableProbe>> getAllProbe(
			ServiceRequest<Void> request) {
		List<IListableProbe> output = new ArrayList<IListableProbe>();

		output = Help.withLists.filterElementOfType(
				diagnostigMgr.getProbeList(), IListableProbe.class);

		return new ServiceResponse<List<IListableProbe>>(output);
	}

}
