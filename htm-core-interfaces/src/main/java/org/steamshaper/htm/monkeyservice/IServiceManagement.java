package org.steamshaper.htm.monkeyservice;

import java.util.List;

import org.steamshaper.htm.monkeyservice.beans.ExecuteActionOnServiceRequest;
import org.steamshaper.htm.monkeyservice.beans.ExecuteActionOnServiceResponse;
import org.steamshaper.htm.monkeyservice.beans.ManagedServiceBean;
import org.steamshaper.htm.service.interfaces.ServiceRequest;
import org.steamshaper.htm.service.interfaces.ServiceResponse;

public interface IServiceManagement {

	ServiceResponse<List<ManagedServiceBean>> getAllManagedService();

	ServiceResponse<ExecuteActionOnServiceResponse> manage(
			ServiceRequest<ExecuteActionOnServiceRequest> request);

}
