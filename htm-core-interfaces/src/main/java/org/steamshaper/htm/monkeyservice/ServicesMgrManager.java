package org.steamshaper.htm.monkeyservice;

import java.util.ArrayList;
import java.util.List;

import org.dozer.Mapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.steamshaper.htm.beans.ManagedService;
import org.steamshaper.htm.handler.IServiceManagementHandler;
import org.steamshaper.htm.monkeyservice.beans.ExecuteActionOnServiceRequest;
import org.steamshaper.htm.monkeyservice.beans.ExecuteActionOnServiceResponse;
import org.steamshaper.htm.monkeyservice.beans.ManagedServiceBean;
import org.steamshaper.htm.service.interfaces.ServiceRequest;
import org.steamshaper.htm.service.interfaces.ServiceResponse;
import org.steamshaper.htm.servicesManager.services.IHTMService;

public class ServicesMgrManager implements IServiceManagement {

	@Autowired
	private IServiceManagementHandler srvMgr;

	@Autowired
	private Mapper mapper;

	@Override
	public ServiceResponse<List<ManagedServiceBean>> getAllManagedService() {
		List<ManagedServiceBean> output = new ArrayList<ManagedServiceBean>();
		for (ManagedService srv : srvMgr.getAllRegisteredServices()) {
			ManagedServiceBean item = mapper.map(srv, ManagedServiceBean.class);
			output.add(item);
		}

		ServiceResponse<List<ManagedServiceBean>> response = new ServiceResponse<List<ManagedServiceBean>>(
				output);
		return response;
	}

	@Override
	public ServiceResponse<ExecuteActionOnServiceResponse> manage(
			ServiceRequest<ExecuteActionOnServiceRequest> request) {
		ServiceResponse<ExecuteActionOnServiceResponse> output = new ServiceResponse<ExecuteActionOnServiceResponse>();
		IHTMService trgService = 	srvMgr.findServiceByName(request.getPayload().getTargetServiceId());
		if(trgService!=null){
			ExecuteActionOnServiceResponse payload = new ExecuteActionOnServiceResponse();
			if(srvMgr.changeStatus(trgService, request.getPayload().getTargetStatus())){
				payload.setMessage("Status Changed");
			}else{
				output.addErrorMessage("Unable to change status!!!");
			}
			output.setPayload(payload);
		}
		return output;
	}

}
