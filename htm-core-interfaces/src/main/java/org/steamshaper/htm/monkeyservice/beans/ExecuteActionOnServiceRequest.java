package org.steamshaper.htm.monkeyservice.beans;

import org.steamshaper.htm.beans.ServiceDescriptor;


public class ExecuteActionOnServiceRequest {
	private String targetServiceId;

	private ServiceDescriptor.STATUS targetStatus;

	public String getTargetServiceId() {
		return targetServiceId;
	}

	public void setTargetServiceId(String targetServiceId) {
		this.targetServiceId = targetServiceId;
	}

	public ServiceDescriptor.STATUS getTargetStatus() {
		return targetStatus;
	}

	public void setTargetStatus(ServiceDescriptor.STATUS targetStatus) {
		this.targetStatus = targetStatus;
	}

}
