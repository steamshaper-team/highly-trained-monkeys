package org.steamshaper.htm.monkeyservice.beans;

public class ExecuteActionOnServiceResponse {

	private String message;

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

}
