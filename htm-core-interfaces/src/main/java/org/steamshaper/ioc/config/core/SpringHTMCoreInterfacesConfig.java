package org.steamshaper.ioc.config.core;

import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.ImportResource;

@Configuration
@ImportResource("classpath:/META-INF/spring/core-interfaces-conf.xml")
public class SpringHTMCoreInterfacesConfig {

}
