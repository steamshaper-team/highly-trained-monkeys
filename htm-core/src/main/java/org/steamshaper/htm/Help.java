package org.steamshaper.htm;

public class Help {

	private Help(){
		
	}
	
	public static final HelpList withLists =  new HelpList(); 
	 
	public static final HelpDignostic withDiagnostic =  new HelpDignostic();
}
