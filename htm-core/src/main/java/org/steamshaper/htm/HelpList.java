package org.steamshaper.htm;

import java.util.ArrayList;
import java.util.List;

public class HelpList {
	
	HelpList(){
		
	}
	
	
	@SuppressWarnings("unchecked")
	public <T> List<T> filterElementOfType(List<?> source,Class<T> clazz){
		List<T> output = new ArrayList<T>();
		for(Object obj:source){
			if(clazz.isInstance(obj)){
				output.add((T) obj);				
			}
		}
		return output;
	
	}

}
