package org.steamshaper.htm.servicesManager.clocks;

import org.steamshaper.htm.servicesManager.services.IHTMScheduledService;
import org.steamshaper.htm.servicesManager.services.IHTMService;



public class FixedRateServiceClock implements IServiceClock {
	
	
	private long rate;
	private long nextRun;
	private IHTMService scheduledService;

	public FixedRateServiceClock(long rate, IHTMScheduledService service) {
		this.rate = rate;
		this.nextRun = System.currentTimeMillis() + rate;
		this.scheduledService = service;
	}

	@Override
	public boolean shouldRun() {
		if(System.currentTimeMillis()>=nextRun){
			nextRun+=rate;
			if(shouldRun()){
				System.err.println("Iteration SKIP "+scheduledService.getServiceDescription().getName());
			}
				return true;
		}
		return false;
	}

}
