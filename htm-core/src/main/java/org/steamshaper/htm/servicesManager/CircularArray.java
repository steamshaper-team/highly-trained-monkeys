package org.steamshaper.htm.servicesManager;

import java.util.Arrays;


public class CircularArray<T> {
	
	
	T[] store;
	
	int cursor =0;
	
	public CircularArray(T[] member){
		store = member;
	}
	
	private CircularArray(T[] newStore, int i) {
		store =  newStore;
		cursor = i;
	}

	public CircularArray<T> add(T[] newMember){
		T[] newStore = Arrays.copyOf(store, store.length+newMember.length);
		for(int i = 0 ;i<newMember.length;i++){
			newStore[store.length+i] =newMember[i]; 
		}
		return new CircularArray<T>(newStore,cursor);
	}
	
	public T next(){
		int index =  getNextIndex();
		return store[index];
	}

	private synchronized int getNextIndex() {
		++cursor;
		if(cursor==store.length){
			cursor=0;
		}
		return cursor;
	}

	public int size(){
		return store.length;
	}
}
