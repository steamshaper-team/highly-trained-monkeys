package org.steamshaper.htm.servicesManager;

public interface IServiceManager {

	public abstract void init();

	public abstract void execute();

}