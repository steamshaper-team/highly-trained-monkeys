package org.steamshaper.htm.servicesManager;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.concurrent.ConcurrentLinkedQueue;

import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.steamshaper.htm.Help;
import org.steamshaper.htm.beans.ManagedService;
import org.steamshaper.htm.beans.ServiceDescriptor;
import org.steamshaper.htm.beans.ServiceDescriptor.SERVICE_TYPE;
import org.steamshaper.htm.beans.ServiceDescriptor.STATUS;
import org.steamshaper.htm.diagnostic.IProbe;
import org.steamshaper.htm.handler.IServiceManagementHandler;
import org.steamshaper.htm.servicesManager.services.IHTMScheduledService;
import org.steamshaper.htm.servicesManager.services.IHTMService;

public class ServicesManager implements ApplicationContextAware,
		IServiceManager, IServiceManagementHandler {

	int counter = 1;
	private ApplicationContext appContext;

	private Map<ServiceDescriptor.SERVICE_TYPE, ArrayList<IHTMService>> servicesTypeMap = new HashMap<>(
			ServiceDescriptor.SERVICE_TYPE.values().length);

	private CircularArray<IHTMScheduledService> scheduledServices;

	private ConcurrentLinkedQueue<IHTMService> runOnceServices = new ConcurrentLinkedQueue<IHTMService>();

	private Map<IHTMService, STATUS> activation = new HashMap<>();

	private boolean noMoreRunOnce = false;

	private IProbe log = Help.withDiagnostic.getProbe("core.services");

	private List<IHTMService> allServicesList = new ArrayList<IHTMService>();

	public ServicesManager() {
		initMaps();
	}

	private void initMaps() {
		for (SERVICE_TYPE type : ServiceDescriptor.SERVICE_TYPE.values()) {
			servicesTypeMap.put(type, new ArrayList<IHTMService>());
		}

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.steamshaper.htm.servicesManager.IServiceManager#init()
	 */
	@Override
	public void init() {

		log.info("Monkey Services INITIALIZATION ...");
		Map<String, IHTMService> services = appContext
				.getBeansOfType(IHTMService.class);

		if (services != null) {
			this.allServicesList.addAll(services.values());
		}

		for (Entry<String, IHTMService> serviceKV : services.entrySet()) {
			IHTMService value = serviceKV.getValue();
			ServiceDescriptor serviceDescription = value
					.getServiceDescription();
			servicesTypeMap.get(serviceDescription.getType()).add(value);
			activation.put(serviceKV.getValue(),
					serviceDescription.getInitialStatus());
			serviceKV.getValue().init();
		}

		ArrayList<IHTMService> scList = servicesTypeMap
				.get(SERVICE_TYPE.SCHEDULED);
		scheduledServices = new CircularArray<IHTMScheduledService>(
				scList.toArray(new IHTMScheduledService[scList.size()]));

		runOnceServices.addAll(servicesTypeMap.get(SERVICE_TYPE.RUN_ONCE));
		log.info("Monkey Services INITIALIZATION ... DONE!");

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.steamshaper.htm.servicesManager.IServiceManager#execute()
	 */
	@Override
	public void execute() {
		if (log.isDebugEnabled()) {
			log.debug("Monkey Services EXECUTE ...");
		}
		IHTMService service = null;
		if (!noMoreRunOnce) {
			service = hasRunOnce();
			noMoreRunOnce = service == null;
		}

		if (service == null) {
			service = pickAServiceToExecute();
		}
		if (service != null && activation.get(service) == STATUS.STARTED) {
			service.execute();
			log.info("Monkey Services EXECUTED ..."
					+ service.getServiceDescription().getName());
		}
		if (log.isDebugEnabled() && service == null) {
			log.debug("Monkey Services EXECUTION ...SKIPPED!");
		}
	}

	@Override
	public List<ManagedService> getAllRegisteredServices() {
		ArrayList<ManagedService> output = new ArrayList<>(
				allServicesList.size());
		ManagedService iterator;
		for (IHTMService srv : allServicesList) {
			iterator = new ManagedService();
			iterator.setActualStatus(this.activation.get(srv));
			iterator.setDescriptor(srv.getServiceDescription());
			output.add(iterator);
		}

		return output;
	}

	@Override
	public void setApplicationContext(ApplicationContext applicationContext)
			throws BeansException {
		this.appContext = applicationContext;

	}

	private synchronized IHTMService hasRunOnce() {
		return runOnceServices.poll();
	}

	private synchronized IHTMService pickAServiceToExecute() {

		for (int i = 0; i < scheduledServices.size(); i++) {
			IHTMScheduledService next = scheduledServices.next();
			if (next.getServiceClock().shouldRun()) {
				return next;
			}
		}

		return null;

	}

	@Override
	public IHTMService findServiceByName(String targetServiceId) {
		IHTMService output = null;
		for (IHTMService srv : allServicesList) {
			if (srv.getServiceDescription().getName().equals(targetServiceId)) {
				output = srv;
				break;
			}
		}
		return output;
	}

	@Override
	public boolean changeStatus(IHTMService target, STATUS newStatus) {
		if (activation.containsKey(target)) {
			activation.put(target, newStatus);
			return true;
		}
		return false;
	}

}
