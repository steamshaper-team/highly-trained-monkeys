package org.steamshaper.htm.servicesManager.clocks;

public interface IServiceClock {
	
	boolean shouldRun();

}
