package org.steamshaper.htm.events;

import java.util.Collection;

import org.steamshaper.htm.events.bean.Event;

public interface IEventsManager {
	void pushEvent(Event event);

	Collection<Event> getAllEvents();
	
	int getEventCount();
}
