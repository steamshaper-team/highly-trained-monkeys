package org.steamshaper.htm.events;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import org.steamshaper.htm.Help;
import org.steamshaper.htm.diagnostic.IProbe;
import org.steamshaper.htm.events.bean.Event;

public class ApplicationEventsManager implements IEventsManager{
	
	
	private int maxEvents;
	
	private ArrayList<Event> eventList;
	
	private IProbe log = Help.withDiagnostic.getProbe("core.events");
	
	
	public ApplicationEventsManager(int maxEvents){
		this.eventList = new ArrayList<Event>(maxEvents);
		this.maxEvents = maxEvents;
	}
	
	
	/* (non-Javadoc)
	 * @see org.steamshaper.htm.model.events.pippo#pushEvent(org.steamshaper.htm.model.events.bean.Event)
	 */
	@Override
	public void pushEvent(Event event){
		if(eventList.size()==maxEvents){
			eventList.remove(0);
		}
		if(event.getInsertDate()==null){
			event.setInsertDate(new Date());
		}
		eventList.add(event);
		log.debug("Event: "+event.getTitle() +" pushed!");
		
	}
	
	/* (non-Javadoc)
	 * @see org.steamshaper.htm.model.events.pippo#getAllEvents()
	 */
	@SuppressWarnings("unchecked")
	@Override
	public Collection<Event> getAllEvents(){
		List<Event> clone =(List<Event>)  eventList.clone();
		Collections.reverse(clone);
		return Collections.unmodifiableList(clone);
	}


	@Override
	public int getEventCount() {
		return this.eventList.size();
	}

}
