package org.steamshaper.htm.events;

import org.springframework.beans.factory.annotation.Autowired;
import org.steamshaper.htm.events.bean.Event;

public abstract class EventPublisher{

	@Autowired
	IEventsManager eventManager;
	
	public void publishEvent(Event event){
		eventManager.pushEvent(event);
	}
	
	
	
	
	
}
