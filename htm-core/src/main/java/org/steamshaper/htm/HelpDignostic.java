package org.steamshaper.htm;

import org.steamshaper.htm.diagnostic.DiagnosticManager;
import org.steamshaper.htm.diagnostic.IDiagnostic;
import org.steamshaper.htm.diagnostic.IProbe;
import org.steamshaper.htm.diagnostic.ListableProbeFactory;

public class HelpDignostic {

	HelpDignostic() {
		this.diagnosticMgr = new DiagnosticManager(true,
				new ListableProbeFactory());
	}

	private IDiagnostic diagnosticMgr;
	private final String ROOT_PROBE = "Root Probe";

	public IProbe getRootProbe() {
		return getProbe(ROOT_PROBE);
	}

	public IProbe getProbe(String name) {
		return diagnosticMgr.getProbeForName(name);
	}
	
	public IDiagnostic getManager(){
		return this.diagnosticMgr;
	}

}
