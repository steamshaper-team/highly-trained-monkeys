package org.steamshaper.htm;

import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;

public class HTMContextListener implements ApplicationContextAware {

	private static ApplicationContext appContext = null;

	@Override
	public void setApplicationContext(ApplicationContext applicationContext)
			throws BeansException {
		if (appContext != null) {
			throw new IllegalStateException(
					"An attempt to change context is happend!");
		}
		appContext = applicationContext;

	}
	
	
	public static <T> T getBeanByClass(Class<T> clazz){
		return getApplicationContext().getBean(clazz);
	}


	private static ApplicationContext getApplicationContext() {
		if(appContext==null){
			throw new IllegalStateException("Application context isn't set!");
		}else{
			return appContext;
		}
		
	}

}
