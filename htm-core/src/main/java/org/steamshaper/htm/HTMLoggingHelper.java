package org.steamshaper.htm;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class HTMLoggingHelper {

	HTMLoggingHelper(){
		
	}
	
	public Logger getLoggingChannelFor(Object object){
		//Dummy strategy
		return  LoggerFactory.getLogger(object.getClass());
	}

	public Logger getLoggingForClass(Class<?> clazz){
		//Dummy strategy
		return  LoggerFactory.getLogger(clazz);
	}
}
