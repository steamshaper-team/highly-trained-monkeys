package org.steamshaper.htm.diagnostic;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.LoggerFactory;
import org.springframework.util.Assert;

public class DiagnosticManager implements IDiagnostic {

	private static final boolean DEFAULT_SEND_TO_LOGGER = true;

	private Map<String, IProbe> probeMap = new HashMap<>();

	private IProbeFactory probeFactory;

	private boolean sendToLogger = DEFAULT_SEND_TO_LOGGER;

	public DiagnosticManager(boolean sendToLogging, IProbeFactory probeFactory) {
		Assert.notNull(probeFactory, "A probe factory must be provided!");
		this.probeFactory = probeFactory;
		this.sendToLogger = sendToLogging;
	}

	public DiagnosticManager(IProbeFactory probeFactory) {
		this(DEFAULT_SEND_TO_LOGGER, probeFactory);
	}

//	public DiagnosticManager() {
//		this(DEFAULT_SEND_TO_LOGGER, DEFAULT_PROBE_FACTORY);
//	}

	/* (non-Javadoc)
	 * @see org.steamshaper.htm.instrumentation.IDiagnostic#getProbeForName(java.lang.String)
	 */
	@Override
	public IProbe getProbeForName(String name) {
		IProbe output = probeMap.get(name);
		if (output == null) {
			output = createProbe(name);
		}
		return output;
	}
	

	private IProbe createProbe(String name) {
		IProbe output = null;
		if (sendToLogger) {
			output = probeFactory.createNew(LoggerFactory.getLogger(name),name);
		} else {
			output = probeFactory.createNew(name);
		}
		probeMap.put(name, output);
		return output;
	}

	
	/* (non-Javadoc)
	 * @see org.steamshaper.htm.instrumentation.IDiagnostic#getProbeList()
	 */
	@Override
	public List<IProbe> getProbeList(){
		return new ArrayList<>(probeMap.values());
	}


}
