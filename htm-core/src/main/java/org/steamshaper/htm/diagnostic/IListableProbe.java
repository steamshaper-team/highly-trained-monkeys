package org.steamshaper.htm.diagnostic;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.steamshaper.htm.ITag;

public interface IListableProbe {

	ProbeMessage[] getLastMessages(int number);
	
	public String getProbeName();
	
	public long getEventsCount();
	
	public long getInfoEvents();
	public long getErrorEvents();
	public long getDebugEvents();
	
	
	public class ProbeMessage{
		
		

		private String message;
		private long timestamp;
		private IListableProbe probe;
		private  int severity;
		private volatile List<ITag> tags;
		
		public ProbeMessage(String message,IListableProbe probe,int severity){
			this.message = message;
			this.probe =  probe;
			this.severity = severity;
			
		}
		
		
		public synchronized void addTag(ITag tag){
			if(tags==null){
				tags = new ArrayList<ITag>(1);
			}
			tags.add(tag);
		}
		
		public List<ITag> getTags(){
			return Collections.unmodifiableList(tags);
		}
		
		public String getMessage() {
			return message;
		}

		public long getTimestamp() {
			return timestamp;
		}
		public IListableProbe getProbe() {
			return probe;
		}
		
		public int getSeverity() {
			return severity;
		}

	}
	
}
 