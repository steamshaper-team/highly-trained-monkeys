package org.steamshaper.htm.diagnostic;

import java.util.List;

public interface IDiagnostic {

	public abstract IProbe getProbeForName(String name);

	public abstract List<IProbe> getProbeList();

}