package org.steamshaper.htm.diagnostic;

import java.util.Stack;

import org.slf4j.Logger;

public class ListableProbe implements IListableProbe, IProbe {

	private static final int DEFAULT_MAX_NUMBER_OF_ELEMENT = 100;
	private int maxElementToKeep = DEFAULT_MAX_NUMBER_OF_ELEMENT;
	private Logger logger;
	private volatile long eventCount = 0L;
	private volatile long infoCount = 0L;
	private volatile long errorCount = 0L;
	private volatile long debugCount = 0L;

	private Stack<IListableProbe.ProbeMessage> messageStack;
	private String probeName;

	public ListableProbe(String name) {
		this(DEFAULT_MAX_NUMBER_OF_ELEMENT, name, null);
	}

	public ListableProbe(Logger logger, String name) {
		this(DEFAULT_MAX_NUMBER_OF_ELEMENT, name, logger);
	}

	public ListableProbe(int maxElementsToKeep, String name) {
		this(maxElementsToKeep, name, null);

	}

	public ListableProbe(int maxElementsToKeep, String name, Logger logger) {
		messageStack = new Stack<IListableProbe.ProbeMessage>();
		messageStack.setSize(maxElementsToKeep);// +5 avoid internal resize
		this.maxElementToKeep = maxElementsToKeep;
		this.logger = logger;
		this.probeName = name;
	}

	@Override
	public void info(String format, Object... strings) {
		infoCount++;
		if (logger != null) {
			logger.info(format, strings);
		}
		ProbeMessage newMessage = new ProbeMessage(String.format(format,
				strings), this, 2);

		pushMessage(newMessage);

	}

	@Override
	public void info(String message) {
		infoCount++;
		if (logger != null) {
			
			logger.info(message);
		}
		ProbeMessage newMessage = new ProbeMessage(message, this, 2);
		pushMessage(newMessage);

	}

	@Override
	public void debug(String format, Object... strings) {
		debugCount++;
		if (logger != null) {
			logger.debug(format, strings);
		}
		ProbeMessage newMessage = new ProbeMessage(String.format(format,
				strings), this, 1);

		pushMessage(newMessage);
	}

	@Override
	public void debug(String message) {
		debugCount++;
		if (logger != null) {
			logger.debug(message);
		}
		ProbeMessage newMessage = new ProbeMessage(message, this, 1);
		pushMessage(newMessage);
	}

	@Override
	public void error(String format, Object... strings) {
		errorCount++;
		if (logger != null) {
			logger.error(format, strings);
		}
		ProbeMessage newMessage = new ProbeMessage(String.format(format,
				strings), this, 0);
		pushMessage(newMessage);

	}

	@Override
	public void error(String message) {
		errorCount++;
		if (logger != null) {
			logger.error(message);
		}
		ProbeMessage newMessage = new ProbeMessage(message, this, 0);

		pushMessage(newMessage);
	}

	@Override
	public boolean isInfoEnabled() {
		if (logger != null) {
			return logger.isInfoEnabled();
		} else {
			return true;
		}
	}

	@Override
	public boolean isDebugEnabled() {
		if (logger != null) {
			return logger.isDebugEnabled();
		} else {
			return true;
		}
	}

	@Override
	public ProbeMessage[] getLastMessages(int number) {
		return messageStack.toArray(new ProbeMessage[Math.min(
				DEFAULT_MAX_NUMBER_OF_ELEMENT, messageStack.size())]);
	}

	@Override
	public String getProbeName() {

		return probeName;
	}

	private synchronized void pushMessage(ProbeMessage newMessage) {
		if (messageStack.size() == maxElementToKeep) {
			messageStack.remove(maxElementToKeep - 1);
		}
		messageStack.push(newMessage);
		event();
	}

	private void event() {
		++eventCount;
		
	}

	@Override
	public long getEventsCount() {
		return this.eventCount;
	}

	@Override
	public long getInfoEvents() {
		return infoCount;
	}

	@Override
	public long getErrorEvents() {
		return errorCount;
	}

	@Override
	public long getDebugEvents() {
		return debugCount;
	}

}
