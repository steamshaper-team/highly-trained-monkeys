package org.steamshaper.htm.diagnostic;

import org.slf4j.Logger;

public class ListableProbeFactory implements IProbeFactory {

	public static final String LOGGER = "logger";
	public static final String MAX_SIZE = "max-size";

	@Override
	public IProbe createNew(String name) {
		return new ListableProbe(name);
	}

	@Override
	public IProbe createNew(Logger logger,String name) {
		return new ListableProbe(logger,name);
	}

}
