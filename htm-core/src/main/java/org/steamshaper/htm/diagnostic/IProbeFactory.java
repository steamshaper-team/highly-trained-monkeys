package org.steamshaper.htm.diagnostic;

import org.slf4j.Logger;

public interface IProbeFactory {
	
	
	public IProbe createNew(Logger logger,String name);
	public IProbe createNew(String name);

}
