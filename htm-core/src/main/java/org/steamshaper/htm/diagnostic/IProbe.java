package org.steamshaper.htm.diagnostic;

public interface IProbe {
	void info(String format,Object...strings);
	void debug(String format,Object...strings);
	void error(String format,Object...strings);
	void info(String message);
	void debug(String message);
	void error(String message);

	boolean isInfoEnabled();
	boolean isDebugEnabled();
}
