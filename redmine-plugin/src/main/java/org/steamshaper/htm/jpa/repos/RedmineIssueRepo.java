package org.steamshaper.htm.jpa.repos;


import org.springframework.data.jpa.repository.JpaRepository;
import org.steamshaper.htm.jpa.entities.redmine.EExternal2RedmineIssue;

public interface RedmineIssueRepo extends JpaRepository<EExternal2RedmineIssue, Long> {

	EExternal2RedmineIssue findByIssueMarker(String extTicketID);
	
	

}
