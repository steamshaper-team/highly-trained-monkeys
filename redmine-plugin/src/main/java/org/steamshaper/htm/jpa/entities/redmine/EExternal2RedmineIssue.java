package org.steamshaper.htm.jpa.entities.redmine;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

	
@Entity
public class EExternal2RedmineIssue {

	@Id
	private Long internalIssueId;

	private Integer redmineIssueId;

	@Column(unique=true)
	private String issueMarker;

	public Long getInternalIssueId() {
		return internalIssueId;
	}

	public void setInternalIssueId(Long internalIssueId) {
		this.internalIssueId = internalIssueId;
	}

	public Integer getRedmineIssueId() {
		return redmineIssueId;
	}

	public void setRedmineIssueId(Integer redmineIssueId) {
		this.redmineIssueId = redmineIssueId;
	}

	public String getIssueMarker() {
		return issueMarker;
	}

	public void setIssueMarker(String issueMarker) {
		this.issueMarker = issueMarker;
	}


}
