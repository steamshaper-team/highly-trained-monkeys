package org.steamshaper.htm.plugin.redmine.handler;

import java.util.List;

import org.dozer.Mapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.steamshaper.htm.beans.redmine.RedmineIssueMapper;
import org.steamshaper.htm.handler.IRedmineHandler;
import org.steamshaper.htm.handler.exception.SteamshaperHandlerRuntimeException;
import org.steamshaper.htm.jpa.entities.redmine.EExternal2RedmineIssue;
import org.steamshaper.htm.jpa.repos.RedmineIssueRepo;

import com.taskadapter.redmineapi.RedmineException;
import com.taskadapter.redmineapi.RedmineManager;
import com.taskadapter.redmineapi.RedmineManager.INCLUDE;
import com.taskadapter.redmineapi.bean.Attachment;
import com.taskadapter.redmineapi.bean.Issue;
import com.taskadapter.redmineapi.bean.IssuePriority;
import com.taskadapter.redmineapi.bean.Tracker;
import com.taskadapter.redmineapi.bean.User;

public class RestRedmineHandler implements IRedmineHandler {

	private String redmineHost = ""; // i.e. "http://134.44.29.193/redmine/"
	private String apiAccessKey = null;
	private Integer queryId = null; // any
	private String userName = null;
	private String password = null;
	private RedmineManager mgr = null;

	@Autowired
	private RedmineIssueRepo redmineIssueRepo;

	@Autowired
	private Mapper beanMapper;

	@Override
	public RedmineIssueMapper findMappingByMarker(String extTicketID) {
		EExternal2RedmineIssue internalMapping = redmineIssueRepo
				.findByIssueMarker(extTicketID);
		RedmineIssueMapper output = null;
		if (internalMapping != null) {
			output = beanMapper.map(internalMapping, RedmineIssueMapper.class,
					"RedmineIssueMapper2EExternal2RedmineIssue");
		}
		return output;
	}

	@Transactional
	public void saveMapping(RedmineIssueMapper mapper) {
		EExternal2RedmineIssue toSave = beanMapper.map(mapper,
				EExternal2RedmineIssue.class,
				"RedmineIssueMapper2EExternal2RedmineIssue");

		redmineIssueRepo.save(toSave);
	}

	@Override
	public User getDefaultUser() {

		try {
			return getMgr().getCurrentUser();
		} catch (RedmineException e) {
			throw new SteamshaperHandlerRuntimeException(
					"Unable to read Default Redmine User", e);
		}
	}

	@Override
	public User getUserById(int userId) {
		try {
			return getMgr().getUserById(userId);
		} catch (RedmineException e) {
			throw new SteamshaperHandlerRuntimeException(
					"Unable to read Redmine User (" + userId + ")", e);
		}
	}

	@Override
	public List<Tracker> getTrackers() {

		try {
			return getMgr().getTrackers();
		} catch (RedmineException e) {
			throw new SteamshaperHandlerRuntimeException(
					"Unable to read Readmine Trackers", e);
		}
	}

	@Override
	public List<IssuePriority> getAllIssuesPriority() {

		try {
			return getMgr().getIssuePriorities();
		} catch (RedmineException e) {
			throw new SteamshaperHandlerRuntimeException(
					"Unable to read Issue Priorities", e);
		}
	}

	public Issue findIssueById(Integer redmineIssueID) {
		return findIssueById(redmineIssueID, INCLUDE.journals,
				INCLUDE.attachments);
	}

	@Override
	public Issue findIssueById(Integer redmineIssueID,
			RedmineManager.INCLUDE... include) {
		try {
			return getMgr().getIssueById(redmineIssueID, include);
		} catch (RedmineException e) {
			throw new SteamshaperHandlerRuntimeException(
					"Unable to read Readmine Trackers", e);
		}
	}

	@Override
	public void updateIssue(Issue existingIssue) {
		try {
			getMgr().update(existingIssue);
		} catch (RedmineException e) {
			throw new SteamshaperHandlerRuntimeException(
					"Unable to Update Issue " + existingIssue.getId(), e);
		}
	}

	@Override
	public Issue createIssue(String projectKey, Issue newIssue) {
		try {
			return getMgr().createIssue(projectKey, newIssue);
		} catch (RedmineException e) {
			throw new SteamshaperHandlerRuntimeException(
					"Unable to create Issue ", e);
		}
	}

	@Override
	public Attachment uploadAttachment(String partName, String mime,
			byte[] payload, int length) {
		
		try {
			return getMgr().uploadAttachment(partName,mime,payload,length);
		} catch (Exception e) {
			throw new SteamshaperHandlerRuntimeException(
					"Unable to upload attachement ["+partName+"]", e);
		}
	}

	@Override
	public List<User> getAllUsers() {
	try {
		return getMgr().getUsers();
	} catch (Exception e) {
		throw new SteamshaperHandlerRuntimeException(
				"Unable to read users list", e);
	}
	}

	private RedmineManager getMgr() {
		if (mgr == null) {
			this.mgr = new RedmineManager(getRedmineHost(), getUserName(),
					getPassword());
		}
		return mgr;
	}

	public String getRedmineHost() {
		return redmineHost;
	}

	public void setRedmineHost(String redmineHost) {
		this.redmineHost = redmineHost;
	}

	public String getApiAccessKey() {
		return apiAccessKey;
	}

	public void setApiAccessKey(String apiAccessKey) {
		this.apiAccessKey = apiAccessKey;
	}

	public Integer getQueryId() {
		return queryId;
	}

	public void setQueryId(Integer queryId) {
		this.queryId = queryId;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

}
