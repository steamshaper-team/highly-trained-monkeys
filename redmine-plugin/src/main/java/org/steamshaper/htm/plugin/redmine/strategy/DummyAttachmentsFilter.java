package org.steamshaper.htm.plugin.redmine.strategy;

import java.util.List;

import org.steamshaper.htm.beans.emails.EMailPart;

public class DummyAttachmentsFilter extends AAttachmentFilter {

	@Override
	public List<EMailPart> filter(List<EMailPart> input) {
		return input;
	}

}
