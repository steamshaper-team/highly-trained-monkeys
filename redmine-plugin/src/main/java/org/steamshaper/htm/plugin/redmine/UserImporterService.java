package org.steamshaper.htm.plugin.redmine;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.steamshaper.htm.beans.ExecutionResults;
import org.steamshaper.htm.beans.ServiceDescriptor;
import org.steamshaper.htm.beans.ServiceDescriptor.SERVICE_TYPE;
import org.steamshaper.htm.beans.ServiceDescriptor.STATUS;
import org.steamshaper.htm.beans.issuemanagement.UserBean;
import org.steamshaper.htm.events.IEventsManager;
import org.steamshaper.htm.events.bean.Event;
import org.steamshaper.htm.handler.IIssuesHandler;
import org.steamshaper.htm.handler.IRedmineHandler;
import org.steamshaper.htm.servicesManager.services.IHTMService;

import com.taskadapter.redmineapi.bean.User;

public class UserImporterService implements IHTMService {

	private ServiceDescriptor sd = null;

	@Autowired
	private IRedmineHandler redmineHandler;

	@Autowired
	private IIssuesHandler issueHandler;

	@Autowired
	private IEventsManager eventManager;

	@Override
	public ExecutionResults execute() {
		List<User> redmineUserList = redmineHandler.getAllUsers();

		List<User> existingUser = new ArrayList<User>();
		List<User> newUsers = new ArrayList<User>();
		for (User rdmUser : redmineUserList) {
			if (issueHandler.existUser(rdmUser.getLogin())) {
				existingUser.add(rdmUser);
			} else {
				UserBean newUser = adaptUser(rdmUser);
				issueHandler.addUser(newUser);
				newUsers.add(rdmUser);
			}
		}
		Event insertEvent = new Event();
		insertEvent.setBody("Found <span class=\"badge\">"+newUsers.size()+"</span> new users.");
		insertEvent.setFontAwesomeGlyph("fa-users");
		insertEvent.setTitle("Redmine user import!");
		insertEvent.setSource("Redmine Users Store");
		insertEvent.setSeverity("success");
		eventManager.pushEvent(insertEvent);

		return null;
	}

	private UserBean adaptUser(User rdmUser) {
		UserBean output = new UserBean();
		output.setCreatedOn(rdmUser.getCreatedOn());
		output.setFirstName(rdmUser.getFirstName());
		output.setLastName(rdmUser.getLastName());
		output.setLogin(rdmUser.getLogin());
		output.setMail(rdmUser.getMail());
		output.setPassword(rdmUser.getPassword());
		return output;
	}

	@Override
	public ServiceDescriptor getServiceDescription() {
		if (sd == null) {
			sd = new ServiceDescriptor();
			sd.setName("Redmine USER Import");
			sd.setType(SERVICE_TYPE.RUN_ONCE);
			sd.setVersion("alpha1");
			sd.setInitialStatus(STATUS.STARTED);
		}
		return sd;
	}

	@Override
	public void init() {
		// TODO Auto-generated method stub

	}

}
