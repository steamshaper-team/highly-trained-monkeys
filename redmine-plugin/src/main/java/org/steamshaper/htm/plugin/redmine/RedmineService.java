package org.steamshaper.htm.plugin.redmine;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.transaction.annotation.Transactional;
import org.steamshaper.htm.HTM;
import org.steamshaper.htm.Help;
import org.steamshaper.htm.beans.ExecutionResults;
import org.steamshaper.htm.beans.ServiceDescriptor;
import org.steamshaper.htm.beans.ServiceDescriptor.SERVICE_TYPE;
import org.steamshaper.htm.beans.ServiceDescriptor.STATUS;
import org.steamshaper.htm.beans.emails.EMail;
import org.steamshaper.htm.beans.emails.EMailPart;
import org.steamshaper.htm.beans.issuemanagement.AttachmentBean;
import org.steamshaper.htm.beans.issuemanagement.IssueBean;
import org.steamshaper.htm.beans.redmine.RedmineIssueMapper;
import org.steamshaper.htm.diagnostic.IProbe;
import org.steamshaper.htm.events.IEventsManager;
import org.steamshaper.htm.events.bean.Event;
import org.steamshaper.htm.handler.IEMailMessageHandler;
import org.steamshaper.htm.handler.IIssuesHandler;
import org.steamshaper.htm.handler.IRedmineHandler;
import org.steamshaper.htm.plugin.redmine.strategy.IAttachmentsFilter;
import org.steamshaper.htm.plugin.redmine.strategy.IIssueUserAssigner;
import org.steamshaper.htm.servicesManager.clocks.FixedRateServiceClock;
import org.steamshaper.htm.servicesManager.clocks.IServiceClock;
import org.steamshaper.htm.servicesManager.services.IHTMScheduledService;

import com.taskadapter.redmineapi.bean.Attachment;
import com.taskadapter.redmineapi.bean.Issue;
import com.taskadapter.redmineapi.bean.Tracker;
import com.taskadapter.redmineapi.bean.User;

public class RedmineService implements IHTMScheduledService {

	@Autowired
	private IRedmineHandler redmineHandler;

	@Autowired
	private IEMailMessageHandler emailHandler;

	@Autowired
	private IIssuesHandler issueHandler;

	// @Autowired
	private IIssueUserAssigner userAssigner;

	@Autowired
	private IAttachmentsFilter attachmentFilter;

	@Autowired
	private IEventsManager eventManager;

	@Value(value = "${version.end.of.life.mode}")
	private boolean isVersionInEndOfLife;

	@Value("${redmine.default.traker}")
	private String defaultTrakerName;

	@Value("${redmine.agent.userid}")
	private int userId = -1;

	private Tracker defaultTraker = null;

	private String projectKey = null;

	private IServiceClock serviceClock = new FixedRateServiceClock(20000L, this);

	private ServiceDescriptor sd;

	private static final Logger LOGGER = HTM.logging
			.getLoggingForClass(RedmineService.class);

	private static final IProbe LOG = Help.withDiagnostic
			.getProbe("services.redmine");

	public RedmineService() {
	}

	@Transactional
	public ExecutionResults execute() {
		doExecution();
		return null;
	}

	private static int i = 0;
	private void doExecution() {
		List<EMail> issueToUpdate = scanEMailSource();
		
		
	
		
		for (EMail message : issueToUpdate) {
			LOGGER.info("Ticket related mail found [" + message.getSubject()
					+ "]");

						String externalID = RedmineH.me.getExternalID(message);

			RedmineIssueMapper mappedIssue = redmineHandler
					.findMappingByMarker(externalID);
			IssueBean internalIssue = null;
			if (mappedIssue != null) {
				internalIssue = issueHandler.findIssueById(mappedIssue
						.getInternalIssue());
			}
			byte[] payload = message.getBody().getPayload();
			if (mappedIssue == null || mappedIssue.getRedmineIssueID() == null) {
				LOGGER.info("New issue  discovered [" + externalID + " ["
						+ message.getSubject() + "]");

				Event ticketEvent = new Event();
				ticketEvent.setBody("New ticket found <b>" + message.getSubject()
						+ "</b>");
				ticketEvent.setTitle("New Ticket!");
				ticketEvent.setFontAwesomeGlyph("fa-bug");
				ticketEvent.setSource("CRM-Mail");
				ticketEvent.setSeverity("danger");
				eventManager.pushEvent(ticketEvent);

				Issue newIssue = new Issue();
				User targetUser = redmineHandler.getUserById(userId);
				if (targetUser == null) {
					targetUser = redmineHandler.getDefaultUser();
				}
				newIssue.setAssignee(targetUser);

				newIssue.setSubject(message.getSubject());
				newIssue.setDescription(new String(payload));

				newIssue.setTracker(getDefaultTraker());

				newIssue.setPriorityId(RedmineH.me.chosePriority(message,
						redmineHandler.getAllIssuesPriority()));

				List<AttachmentBean> uploadAttachements = uploadAttachements(
						message, newIssue, internalIssue);

				newIssue = redmineHandler.createIssue(projectKey, newIssue);

				// persistIssue(extTicketID, returned);
				internalIssue = new IssueBean();
				internalIssue.setDescription(newIssue.getDescription());
				internalIssue.setSubject(newIssue.getSubject());
				internalIssue.setAttachments(uploadAttachements);

				internalIssue = issueHandler.saveIssue(internalIssue);

				createIssueMapper(externalID, newIssue, internalIssue);

			} else {
				Issue existingIssue = redmineHandler.findIssueById(mappedIssue
						.getRedmineIssueID());
				Event ticketEvent = new Event();
				ticketEvent.setBody("Update ticket found <b>" + message.getSubject()
						+ " Redmine ID ["+mappedIssue.getRedmineIssueID()+"]</b>");
				ticketEvent.setTitle("Ticket Update!");
				ticketEvent.setFontAwesomeGlyph("fa-bug");
				ticketEvent.setSource("CRM-Mail");
				ticketEvent.setSeverity("danger");
				eventManager.pushEvent(ticketEvent);
				LOGGER.info("Update issue  discovered [" + externalID + "->"
						+ existingIssue.getId() + "] [" + message.getSubject()
						+ "]");
				String newDescription = existingIssue.getDescription()
						+ "\r\n UPDATE " + (new Date()).toString()
						+ " ------------------\r\n\r\n"
						+(payload!=null?new String(payload):"NONE");

				existingIssue.setDescription(newDescription);
				existingIssue.setPriorityId(RedmineH.me.chosePriority(message,
						redmineHandler.getAllIssuesPriority()));
				redmineHandler.updateIssue(existingIssue);

				internalIssue.setDescription(newDescription);
				List<AttachmentBean> uploadAttachements = uploadAttachements(
						message, existingIssue, internalIssue);
				internalIssue.setAttachments(uploadAttachements);
				issueHandler.saveIssue(internalIssue);

			}
			emailHandler.markAsProcessed(message);
		}
	}

	private List<AttachmentBean> uploadAttachements(EMail message,
			Issue redmineIssue, IssueBean internalIssue) {

		List<EMailPart> messageAttachements = message.getAttachments();

		messageAttachements = attachmentFilter.doFilter(messageAttachements);

		if (internalIssue != null && internalIssue.getAttachments().size() > 0) {
			Map<String, EMailPart> index = new HashMap<>();
			for (EMailPart ma : messageAttachements) {
				index.put(ma.getSha1(), ma);
			}
			for (AttachmentBean ia : internalIssue.getAttachments()) {
				index.remove(ia.getSha1());
			}

			messageAttachements.clear();
			messageAttachements.addAll(index.values());
		}
		List<Attachment> uploadedAttachements = redmineIssue.getAttachments();

		List<AttachmentBean> attBean = new ArrayList<>();
		for (EMailPart att : messageAttachements) {
			Attachment uploadAttachment = redmineHandler.uploadAttachment(
					att.getPartName(), att.getMime(), att.getPayload(),
					att.getPayload().length);

			uploadedAttachements.add(uploadAttachment);

			AttachmentBean ab = new AttachmentBean();
			ab.setContentType(att.getMime());
			ab.setContentURL(uploadAttachment.getContentURL());
			ab.setCreatedOn(uploadAttachment.getCreatedOn());
			ab.setDescription(uploadAttachment.getDescription());
			ab.setFileName(att.getPartName());
			ab.setSha1(att.getSha1());
			ab.setToken(uploadAttachment.getToken());
			ab.setLocalResourcePath(att.getLocalFilePath());
			attBean.add(ab);

		}
		return attBean;
	}

	private void createIssueMapper(String externalID, Issue newIssue,
			IssueBean intIssue) {
		RedmineIssueMapper rim = new RedmineIssueMapper();
		rim.setInternalIssue(intIssue.getId());
		rim.setIssueMarker(externalID);
		rim.setRedmineIssueID(newIssue.getId());

		redmineHandler.saveMapping(rim);
	}

	private List<EMail> scanEMailSource() {

		List<EMail> output = new ArrayList<EMail>();
		// Email channel
		List<EMail> fromEMailCH = emailHandler.getEMailToBeProcessed(1);
		for (EMail email : fromEMailCH) {
			if (RedmineH.me.isAnExternalTicket(email)) {
				output.add(email);
				emailHandler.markAsProcessed(email);
			} else {
				emailHandler.markAsProcessed(email);
			}

		}

		return output;
	}

	private Tracker getDefaultTraker() {
		if (this.defaultTraker == null) {
			for (Tracker traker : redmineHandler.getTrackers()) {
				if (this.defaultTrakerName.equalsIgnoreCase(traker.getName())) {
					this.defaultTraker = traker;
					break;
				}
			}
		}
		return defaultTraker;
	}

	public IIssueUserAssigner getUserAssigner() {
		return userAssigner;
	}

	public void setUserAssigner(IIssueUserAssigner userAssigner) {
		this.userAssigner = userAssigner;
	}

	public String getProjectKey() {
		return projectKey;
	}

	public void setProjectKey(String projectKey) {
		this.projectKey = projectKey;
	}

	@Override
	public ServiceDescriptor getServiceDescription() {
		if (sd == null) {
			sd = new ServiceDescriptor();
			sd.setName("Redmine Connector");
			sd.setType(SERVICE_TYPE.SCHEDULED);
			sd.setVersion("alpha1");
			sd.setInitialStatus(STATUS.STARTED);
		}
		return sd;
	}

	@Override
	public IServiceClock getServiceClock() {
		return this.serviceClock;
	}

	@Override
	public void init() {
		// TODO Auto-generated method stub

	}

}
