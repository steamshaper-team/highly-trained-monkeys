package org.steamshaper.htm.plugin.redmine.strategy;

import java.util.List;

import org.steamshaper.htm.beans.emails.EMailPart;

public class SimpleUserAssignerStrategy implements IIssueUserAssigner {

	private int userID = 1;

	public int getUserID() {
		return userID;
	}

	public void setUserID(int userID) {
		this.userID = userID;
	}

	@Override
	public int assignToUser(String mailTxt, String mailSubject,
			List<EMailPart> attachments) {
		return getUserID();
	}

}
