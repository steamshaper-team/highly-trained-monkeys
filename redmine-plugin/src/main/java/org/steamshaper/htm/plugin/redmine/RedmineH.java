package org.steamshaper.htm.plugin.redmine;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Properties;

import org.slf4j.Logger;
import org.steamshaper.htm.H;
import org.steamshaper.htm.HTM;
import org.steamshaper.htm.beans.emails.EMail;

import com.taskadapter.redmineapi.bean.IssuePriority;

public class RedmineH {
	private static Logger log = HTM.logging.getLoggingChannelFor(RedmineH.class);

	private RedmineH() {

	}

	public static final RedmineH me = new RedmineH();

	public String getExternalID(EMail msg) {
		String output = "";
		if (msg != null && msg.getSubject() != null) {
			output = msg.getSubject()
					.replace("Notifica Assegnazione Ticket ", "").trim();
		}
		return output;
	}

	public boolean isAnExternalTicket(EMail message) {

		return message.getSubject().trim()
				.startsWith("Notifica Assegnazione Ticket T-");
	}

	private boolean initSLAMapping = true;

	private Map<Integer, Integer> priorityMap = new HashMap<Integer, Integer>();

	private Integer defaultRedminePriorityLevel;

	public Integer chosePriority(EMail msg, List<IssuePriority> issuePriorities) {
		if (initSLAMapping) {
			initPriorityMap(issuePriorities);
			initSLAMapping=false;
		}

		Integer output = null;
		
		String body = new String(msg.getBody().getPayload());
		String markerString = "Liv. Gravità:";
		int startMarker = body.indexOf(markerString)+markerString.length();
		int endMarker =  body.indexOf("\n", startMarker);
		
		String dirtyPriority =  body.substring(startMarker, endMarker);
		dirtyPriority = dirtyPriority.substring(0, dirtyPriority.indexOf('-'));
		dirtyPriority =  dirtyPriority.trim();
		try{
			output = priorityMap.get(Integer.parseInt(dirtyPriority));
			if(output==null){
				log.error("Priority level UNKNOW ["+dirtyPriority+"]");
				output = defaultRedminePriorityLevel;
			}
		}catch(NumberFormatException nfe){
			log.error("Unable to cast level ["+dirtyPriority+"] falling back to DEFAULT ["+this.defaultRedminePriorityLevel+"]");
			output = defaultRedminePriorityLevel;
		}
		
		return output;
	}

	private void initPriorityMap(List<IssuePriority> issuePriorities) {
		FileInputStream mappingFile = null;
		try {
			File sla =  H.fsTools.getResource("sla.mapping","conf"); 
			sla.exists();
			System.err.println(sla.getAbsolutePath());
			mappingFile = new FileInputStream(sla);

			Properties prop = new Properties();
			prop.load(mappingFile);

			Enumeration<?> e = prop.propertyNames();

			HashMap<String, String> prioritySet = new HashMap<String, String>();

			// Reading all properties
			while (e.hasMoreElements()) {
				String key = (String) e.nextElement();
				String value = prop.getProperty(key);
				prioritySet.put(key.toLowerCase().trim(), value.trim());
			}

			HashSet<String> levelsSet = new HashSet<String>();
			String defaultPriorityLabel = "";
			for (Entry<String, String> entry : prioritySet.entrySet()) {
				String key = entry.getKey();
				if ("default.priority".equals(key)) {
					defaultPriorityLabel = entry.getValue();
				} else {
					levelsSet.add(key.substring(0, key.indexOf('.')));
				}

			}

			log.debug("Found [" + levelsSet.size() + "] priority level!");

			for ( com.taskadapter.redmineapi.bean.IssuePriority rdmPriority : issuePriorities) {

				//Find priority match in set
				for(String intLevel :levelsSet.toArray(new String[levelsSet.size()])){
				 if(rdmPriority.getName().toLowerCase().contains(intLevel)){
					 //Found
					 this.priorityMap.put(Integer.valueOf(prioritySet.get(intLevel+".ext.code")),rdmPriority.getId());
					 if(intLevel.equalsIgnoreCase(defaultPriorityLabel)){
						 this.defaultRedminePriorityLevel=rdmPriority.getId();
					 }
					 break;
				 }
				}
				
			}

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			if (mappingFile != null) {
				try {
					mappingFile.close();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
	}

}
