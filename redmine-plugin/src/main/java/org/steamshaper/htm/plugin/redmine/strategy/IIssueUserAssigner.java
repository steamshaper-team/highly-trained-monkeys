package org.steamshaper.htm.plugin.redmine.strategy;

import java.util.List;

import org.steamshaper.htm.beans.emails.EMailPart;

public interface IIssueUserAssigner {

	int assignToUser(String mailTxt, String mailSubject, List<EMailPart> attachments);

}
