package org.steamshaper.htm.plugin.redmine.strategy;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

import org.steamshaper.htm.beans.Constants;
import org.steamshaper.htm.beans.emails.EMailPart;

public abstract class AAttachmentFilter implements IAttachmentsFilter {

	private static HashSet<String> baseFilter = new HashSet<String>();
	
	static{
		baseFilter.add(Constants.MESSAGE_BODY_TXT);
		baseFilter.add(Constants.MESSAGE_BODY_HTML);
		baseFilter.add(Constants.MESSAGE_BODY_UNKNOW);
	}

	@Override
	public final List<EMailPart> doFilter(List<EMailPart> input) {
		List<EMailPart> filtered = new ArrayList<EMailPart>(input.size());
		
		for(EMailPart ma : input){
			String toCheck = ma.getPartName();
			if(ma.getPartName().contains(Constants.MESSAGE_BODY_PREFIX)){
				toCheck =  toCheck.substring(toCheck.lastIndexOf(Constants.MESSAGE_BODY_PREFIX));
			}
				
			if(!baseFilter.contains(toCheck)){
				filtered.add(ma);
			}
		}
		
		filtered = this.filter(filtered);
		
		return filtered;
	}

	public abstract List<EMailPart> filter(List<EMailPart> input);

}
