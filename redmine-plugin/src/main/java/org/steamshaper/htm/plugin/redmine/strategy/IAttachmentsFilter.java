package org.steamshaper.htm.plugin.redmine.strategy;

import java.util.List;

import org.steamshaper.htm.beans.emails.EMailPart;

public interface IAttachmentsFilter {
	
	
	List<EMailPart> doFilter(List<EMailPart> input); 

}
