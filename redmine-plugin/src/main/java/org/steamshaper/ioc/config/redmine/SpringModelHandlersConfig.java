package org.steamshaper.ioc.config.redmine;

import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.ImportResource;

@Configuration
@ImportResource("classpath:/META-INF/spring/redmine-plugin-conf.xml")
public class SpringModelHandlersConfig {

}
