package org.steamshaper.htm.plugin.emailpoller;

import java.util.HashSet;

import org.steamshaper.htm.beans.Constants;
import org.steamshaper.htm.beans.emails.EMail;
import org.steamshaper.htm.beans.emails.EMailPart;

public class H {

	private static HashSet<String> bodyName = new HashSet<String>();

	static {
		bodyName.add(Constants.MESSAGE_BODY_TXT);
		bodyName.add(Constants.MESSAGE_BODY_HTML);
		bodyName.add(Constants.MESSAGE_BODY_UNKNOW);
	}
	private static final String FAVOURITE_MIME = Constants.MESSAGE_BODY_TXT;

	public final static H me = new H();

	private H() {
	}

	
	public EMailPart findBody(EMail im) {

		EMailPart output = null;

		for (EMailPart ma : im.getAttachments()) {
			if (bodyName.contains(ma.getPartName())) {
				if (FAVOURITE_MIME.equals(ma.getPartName())) {
					output = ma;
				} else if(output==null) {
					output = ma;
				}
			}
		}

		return output;
	}
	
}
