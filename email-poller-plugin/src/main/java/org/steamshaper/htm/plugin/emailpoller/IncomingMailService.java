package org.steamshaper.htm.plugin.emailpoller;


import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.steamshaper.htm.HTM;
import org.steamshaper.htm.beans.emails.EMail;
import org.steamshaper.htm.events.EventPublisher;
import org.steamshaper.htm.events.IEventsManager;
import org.steamshaper.htm.events.bean.Event;
import org.steamshaper.htm.handler.IEMailMessageHandler;

public class IncomingMailService extends EventPublisher {

	@Autowired
	private IEMailMessageHandler emailHandler;

	@Autowired
	IEventsManager eventManager;
	
	private static final Logger LOGGER = HTM.logging.getLoggingChannelFor(IncomingMailService.class);

	public IncomingMailService() {
	}

	public void handleMail(final EMail message) {
		LOGGER.info("New mail received " + message.getSenders() + "]["
				+ message.getSubject() + "]");
		
		Event event =  new Event();
		event.setBody("New mail received from <b>" + message.getSenders().iterator().next().getAddress() + "</b>: <i>"
				+ message.getSubject() + "</i>");
		event.setTitle("New EMail");
		event.setFontAwesomeGlyph("fa-envelope");
		event.setSource("htm@hpfactory.it poller.");
		eventManager.pushEvent(event);
		emailHandler.registerEmail(message);
		LOGGER.info("New mail received PERSISTED [" + message.getSenders()
				+ "][" + message.getSubject() + "]");

	}

}
