/*
 * Copyright 2002-2012 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.steamshaper.htm.plugin.emailpoller;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.mail.Address;
import javax.mail.BodyPart;
import javax.mail.Message;
import javax.mail.Message.RecipientType;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.Part;
import javax.mail.internet.AddressException;
import javax.mail.internet.ContentType;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MailDateFormat;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.ParseException;

import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.springframework.util.Assert;
import org.steamshaper.htm.HTM;
import org.steamshaper.htm.beans.Constants;
import org.steamshaper.htm.beans.emails.EMail;
import org.steamshaper.htm.beans.emails.EMailAddress;
import org.steamshaper.htm.beans.emails.EMailPart;

/**
 * Utility Class for parsing mail messages.
 * 
 * @author Gunnar Hillert
 * @since 2.2
 * 
 */
public final class EmailParserUtils {

	private static final Logger LOGGER = HTM.logging
			.getLoggingForClass(EmailParserUtils.class);

	/** Prevent instantiation. */
	private EmailParserUtils() {
		throw new AssertionError();
	}

	/**
	 * Parses a mail message. The respective message can either be the root
	 * message or another message that is attached to another message.
	 * 
	 * If the mail message is an instance of {@link String}, then a
	 * {@link EmailFragment} is being created using the email message's subject
	 * line as the file name, which will contain the mail message's content.
	 * 
	 * If the mail message is an instance of {@link Multipart} then we delegate
	 * to {@link #handleMultipart(File, Multipart, javax.mail.Message, List)}.
	 * 
	 * @param directory
	 *            The directory for storing the message. If null this is the
	 *            root message.
	 * @param mailMessage
	 *            The mail message to be parsed. Must not be null.
	 * @param emailFragments
	 *            Must not be null.
	 */
	public static EMail handleMessage(final javax.mail.Message mailMessage) {

		Assert.notNull(mailMessage,
				"The mail message to be parsed must not be null.");
		EMail im = new EMail();

		final Object content;

		try {
			content = mailMessage.getContent();
			im.setSubject(mailMessage.getSubject());
			im.setRecipients(readRecipients(mailMessage));
			im.setSenders(convertToAddressBean(mailMessage.getFrom()));
			String[] header = mailMessage.getHeader("Date");
			if (header != null && header.length > 0) {
				im.setSendDate(dateString2Date(header[0]));
			}
			mailMessage.getAllHeaders();

		} catch (IOException e) {
			throw new IllegalStateException(
					"Error while retrieving the email contents.", e);
		} catch (MessagingException e) {
			throw new IllegalStateException(
					"Error while retrieving the email contents.", e);
		}

		if (content instanceof String) {
			EMailPart bodyPart = new EMailPart();
			bodyPart.setMime("text/plain");
			bodyPart.setPartName(Constants.MESSAGE_BODY_TXT);
			bodyPart.setPayload(((String) content).getBytes());

			im.setBody(bodyPart);
		} else if (content instanceof Multipart) {
			Multipart multipart = (Multipart) content;
			im = handleMultipart(multipart, mailMessage, im);
			im.setBody(H.me.findBody(im));
		} else {
			throw new IllegalStateException(
					"This content type is not handled - "
							+ content.getClass().getSimpleName());
		}

		return im;

	}

	public static Set<EMailAddress> readRecipients(
			final javax.mail.Message mailMessage) throws MessagingException {
		Set<EMailAddress> addressBeans = null;
		try {
			 addressBeans =
			 convertToAddressBean(mailMessage.getAllRecipients());
		} catch (AddressException aex) {
			// / fall back strategy, some address may be not strictly valid
			Address[] to = null;
			Address[] cc = null;
			Address[] bcc = null;

			try {
				to = mailMessage.getRecipients(RecipientType.TO);
			} catch (AddressException aexTo) {
				String toS = joinInAString(
						mailMessage.getHeader(getHeaderName(RecipientType.TO)),
						",");

				LOGGER.error("Malformed recipient DUMP["+toS+"]");
				
				toS = filterInvalidChar(toS);
				try {
					to = InternetAddress.parseHeader(toS, true);
				} catch (Exception e) {
					//Do nothing for the moment
				}

			}

			try {
				cc = mailMessage.getRecipients(RecipientType.CC);
			} catch (AddressException aexCc) {
				String ccS = joinInAString(
						mailMessage.getHeader(getHeaderName(RecipientType.CC)),
						",");
				LOGGER.error("Malformed recipient DUMP["+ccS+"]");
				ccS = filterInvalidChar(ccS);
			try{
				cc = InternetAddress.parseHeader(ccS, true);
			} catch (Exception e) {
				//Do nothing for the moment
			}
			}

			try {
				bcc = mailMessage.getRecipients(RecipientType.BCC);
			} catch (AddressException aexBcc) {
				String bccS = joinInAString(
						mailMessage.getHeader(getHeaderName(RecipientType.BCC)),
						",");
				LOGGER.error("Malformed recipient DUMP["+bccS+"]");
				bccS = filterInvalidChar(bccS);
			try{
				bcc = InternetAddress.parseHeader(bccS, true);
			} catch (Exception e) {
				//Do nothing for the moment
			}
			}

			Address[] addresses = null;
			if (cc == null && bcc == null) {
				addresses = to; // a common case
			} else {

				int numRecip = (to != null ? to.length : 0)
						+ (cc != null ? cc.length : 0)
						+ (bcc != null ? bcc.length : 0);
				addresses = new Address[numRecip];
				int pos = 0;
				if (to != null) {
					System.arraycopy(to, 0, addresses, pos, to.length);
					pos += to.length;
				}
				if (cc != null) {
					System.arraycopy(cc, 0, addresses, pos, cc.length);
					pos += cc.length;
				}
				if (bcc != null) {
					System.arraycopy(bcc, 0, addresses, pos, bcc.length);
					// pos += bcc.length;
				}
			}
			addressBeans = convertToAddressBean(addresses);
		}
		return addressBeans;
	}

	private static String filterInvalidChar(String toS) {
		if (toS == null) {
			return null;
		}
		return toS.replaceAll("'", "").replaceAll("_", "");
	}

	private static String joinInAString(String[] s, String delimiter) {

		if (s == null)
			return null;

		if ((s.length == 1) || delimiter == null)
			return s[0];

		StringBuffer r = new StringBuffer(s[0]);
		for (int i = 1; i < s.length; i++) {
			r.append(delimiter);
			r.append(s[i]);
		}
		return r.toString();

	}

	private static Date dateString2Date(String dateString) {
		Date output = new Date(0L);
		if (dateString != null && dateString.trim().length() > 0) {
			try {
				output = (new MailDateFormat()).parse(dateString);
			} catch (java.text.ParseException e) {
			}
		}
		return output;
	}

	private static Set<EMailAddress> convertToAddressBean(Address[] allAddress) {
		Set<EMailAddress> output = new HashSet<EMailAddress>();

		EMailAddress temp;
		for (Address address : allAddress) {
			InternetAddress inetAddr = (InternetAddress) address;
			temp = new EMailAddress();
			output.add(temp);
			temp.setAddress(inetAddr.getAddress());
		}
		return output;
	}

	/**
	 * Parses any {@link Multipart} instances that contain text or Html
	 * attachments, {@link InputStream} instances, additional instances of
	 * {@link Multipart} or other attached instances of
	 * {@link javax.mail.Message}.
	 * 
	 * Will create the respective {@link EmailFragment}s representing those
	 * attachments.
	 * 
	 * Instances of {@link javax.mail.Message} are delegated to
	 * {@link #handleMessage(File, javax.mail.Message, List)}. Further instances
	 * of {@link Multipart} are delegated to
	 * {@link #handleMultipart(File, Multipart, javax.mail.Message, List)}.
	 * 
	 * @param directory
	 *            Must not be null
	 * @param multipart
	 *            Must not be null
	 * @param mailMessage
	 *            Must not be null
	 * @param emailFragments
	 *            Must not be null
	 */
	public static EMail handleMultipart(Multipart multipart,
			javax.mail.Message mailMessage, EMail mail) {

		Assert.notNull(multipart,
				"The multipart object to be parsed must not be null.");
		Assert.notNull(mailMessage,
				"The mail message to be parsed must not be null.");

		final int count;

		try {
			count = multipart.getCount();

			if (LOGGER.isInfoEnabled()) {
				LOGGER.info(String.format(
						"Number of enclosed BodyPart objects: %s.", count));
			}

		} catch (MessagingException e) {
			throw new IllegalStateException(
					"Error while retrieving the number of enclosed BodyPart objects.",
					e);
		}

		for (int i = 0; i < count; i++) {

			final BodyPart bp;

			try {
				bp = multipart.getBodyPart(i);
			} catch (MessagingException e) {
				throw new IllegalStateException(
						"Error while retrieving body part.", e);
			}

			final String contentType;
			String filename;
			final String disposition;
			final String subject;

			try {

				contentType = bp.getContentType();
				filename = bp.getFileName();
				disposition = bp.getDisposition();
				subject = mailMessage.getSubject();

				if (filename == null && bp instanceof MimeBodyPart) {
					filename = ((MimeBodyPart) bp).getContentID();
				}

			} catch (MessagingException e) {
				throw new IllegalStateException(
						"Unable to retrieve body part meta data.", e);
			}

			if (LOGGER.isInfoEnabled()) {
				LOGGER.info(String
						.format("BodyPart - Content Type: '%s', filename: '%s', disposition: '%s', subject: '%s'",
								new Object[] { contentType, filename,
										disposition, subject }));
			}

			if (Part.ATTACHMENT.equalsIgnoreCase(disposition)) {
				LOGGER.info(String.format(
						"Handdling attachment '%s', type: '%s'", filename,
						contentType));
			}

			final Object content;

			try {
				content = bp.getContent();
			} catch (IOException e) {
				throw new IllegalStateException(
						"Error while retrieving the email contents.", e);
			} catch (MessagingException e) {
				throw new IllegalStateException(
						"Error while retrieving the email contents.", e);
			}

			if (content instanceof String) {

				if (Part.ATTACHMENT.equalsIgnoreCase(disposition)) {
					mail.getAttachments().add(
							new EMailPart(filename, ((String) content)
									.getBytes(), "text/plain"));
					LOGGER.info(String.format(
							"Handdling attachment '%s', type: '%s'", filename,
							contentType));
				} else {

					final String textFilename;
					final ContentType ct;

					try {
						ct = new ContentType(contentType);
					} catch (ParseException e) {
						throw new IllegalStateException(
								"Error while parsing content type '"
										+ contentType + "'.", e);
					}

					if ("text/plain".equalsIgnoreCase(ct.getBaseType())) {
						textFilename = Constants.MESSAGE_BODY_TXT;
					} else if ("text/html".equalsIgnoreCase(ct.getBaseType())) {
						textFilename = Constants.MESSAGE_BODY_HTML;
					} else {
						textFilename = Constants.MESSAGE_BODY_UNKNOW;
					}
					mail.getAttachments()
							.add(new EMailPart(textFilename, ((String) content)
									.getBytes(), ct.getBaseType().toLowerCase()));
				}

			} else if (content instanceof InputStream) {

				final InputStream inputStream = (InputStream) content;
				final ByteArrayOutputStream bis = new ByteArrayOutputStream();

				try {
					IOUtils.copy(inputStream, bis);
				} catch (IOException e) {
					throw new IllegalStateException(
							"Error while copying input stream to the ByteArrayOutputStream.",
							e);
				}

				mail.getAttachments().add(
						new EMailPart(filename, bis.toByteArray(),
								"application/octet-stream"));

			} else if (content instanceof javax.mail.Message) {

				EMail inner = handleMessage((javax.mail.Message) content);
				List<EMailPart> innerAttachments = new ArrayList<EMailPart>(
						inner.getAttachments().size());
				for (EMailPart attachement : inner.getAttachments()) {
					innerAttachments.add(new EMailPart(inner.getSubject() + "-"
							+ attachement.getPartName(), attachement
							.getPayload(), attachement.getMime()));
				}
				mail.getAttachments().addAll(innerAttachments);
			} else if (content instanceof Multipart) {
				final Multipart mp2 = (Multipart) content;
				EMail inner = handleMultipart(mp2, mailMessage, mail);
				List<EMailPart> innerAttachments = new ArrayList<EMailPart>(
						inner.getAttachments().size());

				for (EMailPart attachement : inner.getAttachments()) {
					innerAttachments.add(new EMailPart(inner.getSubject() + "-"
							+ attachement.getPartName(), attachement
							.getPayload(), attachement.getMime()));
				}
				mail.getAttachments().addAll(innerAttachments);
			} else {
				throw new IllegalStateException("Content type not handled: "
						+ content.getClass().getSimpleName());
			}
		}

		return mail;
	}

	private static String getHeaderName(Message.RecipientType type) {
		String headerName = null;

		if (type == Message.RecipientType.TO)
			headerName = "To";
		else if (type == Message.RecipientType.CC)
			headerName = "Cc";
		else if (type == Message.RecipientType.BCC)
			headerName = "Bcc";
		else if (type == MimeMessage.RecipientType.NEWSGROUPS)
			headerName = "Newsgroups";

		return headerName;
	}

}
