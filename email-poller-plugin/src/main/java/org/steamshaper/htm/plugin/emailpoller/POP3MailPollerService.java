package org.steamshaper.htm.plugin.emailpoller;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import javax.mail.Address;
import javax.mail.BodyPart;
import javax.mail.Flags;
import javax.mail.Folder;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.NoSuchProviderException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Store;
import javax.mail.internet.MimeMessage;

import org.springframework.beans.factory.annotation.Autowired;
import org.steamshaper.htm.beans.ExecutionResults;
import org.steamshaper.htm.beans.ServiceDescriptor;
import org.steamshaper.htm.beans.ServiceDescriptor.SERVICE_TYPE;
import org.steamshaper.htm.beans.ServiceDescriptor.STATUS;
import org.steamshaper.htm.handler.exception.SteamshaperHandlerRuntimeException;
import org.steamshaper.htm.servicesManager.clocks.FixedRateServiceClock;
import org.steamshaper.htm.servicesManager.clocks.IServiceClock;
import org.steamshaper.htm.servicesManager.services.IHTMScheduledService;

public class POP3MailPollerService implements IHTMScheduledService {

	@Autowired
	private IncomingMailService mailPersisterService;
	private ServiceDescriptor sd;

	private Properties properties = null;
	private Session session = null;
	private Store store = null;
	private Folder inbox = null;
	private String userName = "";// provide user name
	private String password = "";// provide password
	private String pop3Host = null;
	private Integer pop3Port = null;
	private String socketFactoryClass = null;
	private Boolean socketFactoryFallback = null;
	private Integer socketFactoryPort = null;

	private int maxFetchSize = 1;

	private Integer pop3ConnectionTimeout;
	private Integer pop3Timeout;
	private Integer pop3WriteTimeout;

	@Override
	public ExecutionResults execute() {

		session = getSession();
		try {
			inbox = getInbox();
			inbox.open(Folder.READ_WRITE);
			Message messages[] = inbox.getMessages();
			for (int i = 0; i < Math.min(messages.length, maxFetchSize); i++) {
				Message message = messages[i];
				Address[] from = message.getFrom();
				System.out.println("-------------------------------");
				System.out.println("Date : " + message.getSentDate());
				System.out.println("From : " + from[0]);
				System.out.println("Subject: " + message.getSubject());
				// System.out.println("Content :");
				// processMessageBody(message);
				System.out.println("--------------------------------");
				mailPersisterService.handleMail(EmailParserUtils
						.handleMessage(message));
				messages[i].setFlag(Flags.Flag.DELETED, true);
				// expunge deleted mails, and make sure we've retrieved them before closing the folder
				new MimeMessage((MimeMessage) message); 

			}
		} catch (NoSuchProviderException e) {
			e.printStackTrace();
		} catch (MessagingException e) {
			e.printStackTrace();
		} finally {

			try {
				inbox.close(true);
			} catch (MessagingException e) {
				e.printStackTrace();
			}
		}
		return null;
	}

	private Folder getInbox() throws MessagingException {
		if (store == null) {
			store = session.getStore("pop3");
			store.connect();
		}

		return store.getFolder("INBOX");
	}

	private Session getSession() {
		if (session == null) {
			session = Session.getInstance(getProperties(),
					new javax.mail.Authenticator() {
						protected PasswordAuthentication getPasswordAuthentication() {
							return new PasswordAuthentication(userName,
									password);
						}
					});
		}
		return session;
	}

	public void processMessageBody(Message message) {
		try {
			Object content = message.getContent();
			// check for string
			// then check for multipart
			if (content instanceof String) {
				System.out.println(content);
			} else if (content instanceof Multipart) {
				Multipart multiPart = (Multipart) content;
				procesMultiPart(multiPart);
			} else if (content instanceof InputStream) {
				InputStream inStream = (InputStream) content;
				int ch;
				while ((ch = inStream.read()) != -1) {
					System.out.write(ch);
				}
				inStream.close();
			}
		} catch (IOException e) {
			e.printStackTrace();
		} catch (MessagingException e) {
			e.printStackTrace();
		}
	}

	/**
	 *   * recursively processes multipart messages   *   * @param content  
	 */
	public void procesMultiPart(Multipart content) {
		try {
			int multiPartCount = content.getCount();
			for (int i = 0; i < multiPartCount; i++) {
				BodyPart bodyPart = content.getBodyPart(i);
				Object o;
				o = bodyPart.getContent();
				if (o instanceof String) {
					System.out.println(o);
				} else if (o instanceof Multipart) {
					procesMultiPart((Multipart) o);
				}
			}
		} catch (IOException e) {
			e.printStackTrace();
		} catch (MessagingException e) {
			e.printStackTrace();
		}
	}

	@Override
	public ServiceDescriptor getServiceDescription() {
		if (sd == null) {
			sd = new ServiceDescriptor();
			sd.setName("EMAIL Poller");
			sd.setType(SERVICE_TYPE.SCHEDULED);
			sd.setVersion("alpha1");
			sd.setInitialStatus(STATUS.STARTED);
		}
		return sd;
	}

	@Override
	public void init() {
		// setProperties(this.appContext.getBean("javaMailProperties",
		// Properties.class));
		// receiver = new Pop3MailReceiver(getStoreUri());
		// receiver.setMaxFetchSize(1);
		// this.mailPersisterService = this.appContext
		// .getBean(IncomingMailService.class);
		// receiver.setJavaMailProperties();
		// receiver.setShouldDeleteMessages(true);
		// receiver.setApplicationContext(this.appContext);

	}

	private IServiceClock serviceClock = new FixedRateServiceClock(30000L, this);

	@Override
	public IServiceClock getServiceClock() {
		return this.serviceClock;
	}

	public Properties getProperties() {
		if (properties == null) {
			properties = initProperties();
		}
		return properties;
	}

	private Properties initProperties() {
		Properties properties = new Properties();
		if (this.socketFactoryClass != null) {
			properties.setProperty("mail.pop3.socketFactory.class",
					this.socketFactoryClass);
		}
		if (this.socketFactoryFallback != null) {
			properties.setProperty("mail.pop3.socketFactory.fallback",
					this.socketFactoryFallback.toString());
		}
		if (pop3Host == null || pop3Port == null) {
			throw new SteamshaperHandlerRuntimeException(
					"Unable to read POP3 Host OR Port");
		} else {
			properties.setProperty("mail.pop3.host", this.pop3Host);
			properties.setProperty("mail.pop3.port", this.pop3Port.toString());
		}
		if (socketFactoryPort != null) {
			properties.setProperty("mail.pop3.socketFactory.port",
					this.socketFactoryPort.toString());
		}
		if (pop3ConnectionTimeout != null && pop3ConnectionTimeout > 0) {
			properties.setProperty("mail.pop3.connectiontimeout",
					pop3ConnectionTimeout.toString());
		}
		if (pop3Timeout != null && pop3Timeout > 0) {
			properties.setProperty("mail.pop3.timeout", pop3Timeout.toString());
		}
		if (pop3WriteTimeout != null && pop3WriteTimeout > 0) {
			properties.setProperty("mail.pop3.writetimeout",
					pop3WriteTimeout.toString());
		}

		properties.setProperty("mail.debug", "true");
		return properties;
	}

	public void setProperties(Properties properties) {
		this.properties = properties;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getPop3Host() {
		return pop3Host;
	}

	public void setPop3Host(String pop3Host) {
		this.pop3Host = pop3Host;
	}

	public Integer getPop3Port() {
		return pop3Port;
	}

	public void setPop3Port(Integer pop3Port) {
		this.pop3Port = pop3Port;
	}

	public String getSocketFactoryClass() {
		return socketFactoryClass;
	}

	public void setSocketFactoryClass(String socketFactoryClass) {
		this.socketFactoryClass = socketFactoryClass;
	}

	public Boolean getSocketFactoryFallback() {
		return socketFactoryFallback;
	}

	public void setSocketFactoryFallback(Boolean socketFactoryFallback) {
		this.socketFactoryFallback = socketFactoryFallback;
	}

	public Integer getSocketFactoryPort() {
		return socketFactoryPort;
	}

	public void setSocketFactoryPort(Integer socketFactoryPort) {
		this.socketFactoryPort = socketFactoryPort;
	}

	public int getMaxFetchSize() {
		return maxFetchSize;
	}

	public void setMaxFetchSize(int maxFetchSize) {
		this.maxFetchSize = maxFetchSize;
	}

	public void setPop3ConnectionTimeout(Integer arg0) {
		this.pop3ConnectionTimeout = arg0;

	}

	public void setPop3Timeout(Integer arg0) {
		this.pop3Timeout = arg0;

	}

	public void setPop3WriteTimeout(Integer arg0) {
		this.pop3WriteTimeout = arg0;

	}

}