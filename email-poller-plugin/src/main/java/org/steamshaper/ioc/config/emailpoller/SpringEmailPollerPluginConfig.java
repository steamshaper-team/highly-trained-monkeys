package org.steamshaper.ioc.config.emailpoller;

import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.ImportResource;

@Configuration
@ImportResource("classpath:/META-INF/spring/mail-poller-plugin-conf.xml")
public class SpringEmailPollerPluginConfig {

}
