package org.steamshaper.htm.jpa;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

import javax.transaction.Transactional;

import org.dozer.Mapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.steamshaper.htm.beans.emails.EMail;
import org.steamshaper.htm.beans.emails.EMailPart;
import org.steamshaper.htm.handler.IEMailMessageHandler;
import org.steamshaper.htm.jpa.entities.mail.EAddress;
import org.steamshaper.htm.jpa.entities.mail.EEMail;
import org.steamshaper.htm.jpa.repos.EAddressRepo;
import org.steamshaper.htm.jpa.repos.EMailRepo;

public class EMailMessageHandler implements IEMailMessageHandler {

	@Autowired
	private EMailRepo emailRepository;
	@Autowired
	private EAddressRepo addressRepo;

	/** The bean mapper. */
	@Autowired
	private Mapper beanMapper;

	
	public static final String MESSAGE_BODY_PREFIX = "__message";
	public static final String MESSAGE_BODY_TXT = MESSAGE_BODY_PREFIX + ".txt";
	public static final String MESSAGE_BODY_HTML = MESSAGE_BODY_PREFIX
			+ ".html";
	public static final String MESSAGE_BODY_UNKNOW = MESSAGE_BODY_PREFIX
			+ ".unknown";
	private static HashSet<String> bodyName = new HashSet<String>();

	static {
		bodyName.add(MESSAGE_BODY_TXT);
		bodyName.add(MESSAGE_BODY_HTML);
		bodyName.add(MESSAGE_BODY_UNKNOW);
	}
	private String favouriteBodyMime =MESSAGE_BODY_TXT;
	
	@Override
	@Transactional
	public void registerEmail(EMail message) {

		EEMail mailEntity = beanMapper.map(message, EEMail.class,
				"email2emailEntity");

		List<EAddress> senders = addressRepo.save(mailEntity.getSenders());
		List<EAddress> recipients = addressRepo
				.save(mailEntity.getRecipients());
		mailEntity.setSenders(new HashSet<EAddress>(senders));
		mailEntity.setRecipients(new HashSet<EAddress>(recipients));
		emailRepository.save(mailEntity);

	}

	@Override
	public List<EMail> getEMailToBeProcessed() {
		List<EMail> outputMail = new ArrayList<EMail>();
		for (EEMail email : emailRepository.findAllByProcessingTimestampIs(-1L)) {
			EMail emailBean = mapEntity2Bean(email);
			emailBean = findBodyAnSet(emailBean);
			outputMail.add(emailBean);
		}
		return outputMail;
	}

	@Override
	@Transactional
	public List<EMail> getEMailToBeProcessed(int maxResults) {
		List<EMail> outputMail = new ArrayList<EMail>();
		for (EEMail email : emailRepository.findByProcessingTimestampIs(-1L,
				new PageRequest(0, maxResults))) {
			EMail emailBean = mapEntity2Bean(email);
			emailBean = findBodyAnSet(emailBean);
			outputMail.add(emailBean);
			
		}
		return outputMail;
	}

	@Override
	public EMail getFirstMailToBeProcessed() {
		EMail emailBean = null;
		for (EEMail email : emailRepository.findByProcessingTimestampIs(-1L,
				new PageRequest(0, 1))) {
			emailBean = mapEntity2Bean(email);
			emailBean = findBodyAnSet(emailBean);
		}
		return emailBean;
	}

	@Transactional
	public void saveMail(EMail mailToSave) {
		EEMail toSave = beanMapper.map(mailToSave, EEMail.class);
		emailRepository.save(toSave);
	}

	@Transactional
	public void saveMails(List<EMail> mailsToSave) {

		List<EEMail> toSave = new ArrayList<>();
		for (EMail mail : mailsToSave) {
			toSave.add(beanMapper.map(mail, EEMail.class));
		}
		emailRepository.save(toSave);
	}

	@Override
	@Transactional
	public void markAsProcessed(EMail email) {
		EEMail findOne = emailRepository.findOne(email.getInternalId());
		findOne.setProcessingTimestamp(
				System.currentTimeMillis());
		emailRepository.save(findOne);

	}



	public String getFavouriteBodyMime() {
		return favouriteBodyMime;
	}

	public void setFavouriteBodyMime(String favouriteBodyMime) {
		this.favouriteBodyMime = favouriteBodyMime;
	}

	private EMail findBodyAnSet(EMail input) {

		EMailPart output = null;

		for (EMailPart ma : input.getAttachments()) {
			if (bodyName.contains(ma.getPartName())){
				if (getFavouriteBodyMime().equals(ma.getPartName())) {
					output = ma;
				} else if(output==null) {
					output = ma;
				}
			}
		}
		input.setBody(output);
		return input;
	}
	
	
	
	private EMail mapEntity2Bean(EEMail email) {
		return beanMapper.map(email, EMail.class,
				"email2emailEntity");
	}



	


}
