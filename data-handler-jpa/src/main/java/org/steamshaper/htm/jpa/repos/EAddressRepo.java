package org.steamshaper.htm.jpa.repos;

import org.springframework.data.jpa.repository.JpaRepository;
import org.steamshaper.htm.jpa.entities.mail.EAddress;

public interface EAddressRepo extends JpaRepository<EAddress, String> {

}
