package org.steamshaper.htm.jpa;

import javax.transaction.Transactional;

import org.dozer.Mapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.steamshaper.htm.beans.issuemanagement.IssueBean;
import org.steamshaper.htm.beans.issuemanagement.UserBean;
import org.steamshaper.htm.handler.IIssuesHandler;
import org.steamshaper.htm.handler.exception.SteamshaperHandlerRuntimeException;
import org.steamshaper.htm.jpa.entities.issuemanagement.EIssue;
import org.steamshaper.htm.jpa.entities.issuemanagement.EUser;
import org.steamshaper.htm.jpa.repos.EIssueRepo;
import org.steamshaper.htm.jpa.repos.EUserRepo;

@Service
public class IssuesHandler implements IIssuesHandler {

	@Autowired
	private EIssueRepo issueRepo;

	@Autowired
	private EUserRepo userRepo;

	@Autowired
	private Mapper mapper;

	@Override
	@Transactional
	public IssueBean saveIssue(IssueBean intIssue) {
		EIssue findOne = null;
		if (intIssue.getId() == null) {
			findOne = new EIssue();
		} else {
			findOne = issueRepo.findOne(intIssue.getId());
		}
		mapper.map(intIssue, findOne, "IssueBean2EIssue");
		findOne = issueRepo.save(findOne);
		return mapper.map(findOne, IssueBean.class, "IssueBean2EIssue");
	}

	@Override
	public IssueBean findIssueById(long internalIssue) {
		EIssue issueEntity = issueRepo.findOne(internalIssue);
		return mapper.map(issueEntity, IssueBean.class, "IssueBean2EIssue");
	}

	@Override
	@Transactional
	public UserBean addUser(UserBean user) {
		if (user == null || user.getLogin() == null
				|| user.getLogin().isEmpty()) {
			throw new SteamshaperHandlerRuntimeException(
					"User must not be null!");
		}
		user.setLogin(user.getLogin().trim());
		if (userRepo.exists(user.getLogin())) {
			throw new SteamshaperHandlerRuntimeException("User ["
					+ user.toString() + "] ALREADY EXIST!");
		}

		EUser newUser = userRepo.save(mapper.map(user, EUser.class));
		mapper.map(newUser, user);
		return user;
	}

	@Override
	@Transactional
	public UserBean findUserByLoginName(String username) {
		EUser findOne = userRepo.findOne(username);
		UserBean output =  null;
		if (findOne != null) {
			output = mapper.map(findOne, UserBean.class);
		}
		return output;
	}

	@Override
	@Transactional
	public boolean existUser(String login) {
		
		return userRepo.exists(login.trim());
	}

}
