package org.steamshaper.htm.jpa.entities.mail;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;

@Entity
public class EEMail {

	public Date getReadTimeStamp() {
		return readTimeStamp;
	}


	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Long id;
	
	private Long processingTimestamp=-1L;
	
	private String subject;
	
	private Date readTimeStamp =  new Date();
	
	@ManyToMany(cascade={CascadeType.PERSIST})
	@JoinTable(name = "mails_recipients")
	private Set<EAddress> recipients;
	
	@ManyToMany(cascade={CascadeType.PERSIST})
	@JoinTable(name = "mails_senders")
	private Set<EAddress> senders;
	
	@OneToMany(cascade=CascadeType.ALL)
	private List<EMailAttachments> attachments;



	private Date sendDate;
	
	public void setSubject(String subject) {
		this.subject =  subject;
	}

	

	public List<EMailAttachments> getAttachments() {
		if(attachments==null){
			attachments =  new ArrayList<EMailAttachments>();
		}
		return attachments;
	}

	public void setAttachments(List<EMailAttachments> attachments) {
		this.attachments = attachments;
	}

	public String getSubject() {
		return this.subject;
	}

	
	public Long getId() {
		return id;
	}


	public Set<EAddress> getRecipients() {
		return recipients;
	}


	public Set<EAddress> getSenders() {
		return senders;
	}


	public void setRecipients(Set<EAddress> recipients) {
		this.recipients = recipients;
	}


	public void setSenders(Set<EAddress> senders) {
		this.senders = senders;
	}


	public Long getProcessingTimestamp() {
		return processingTimestamp;
	}


	public void setProcessingTimestamp(Long processingTimestamp) {
		this.processingTimestamp = processingTimestamp;
	}


	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime
				* result
				+ ((processingTimestamp == null) ? 0 : processingTimestamp
						.hashCode());
		result = prime * result
				+ ((recipients == null) ? 0 : recipients.hashCode());
		result = prime * result + ((senders == null) ? 0 : senders.hashCode());
		result = prime * result + ((subject == null) ? 0 : subject.hashCode());
		return result;
	}


	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		EEMail other = (EEMail) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (processingTimestamp == null) {
			if (other.processingTimestamp != null)
				return false;
		} else if (!processingTimestamp.equals(other.processingTimestamp))
			return false;
		if (recipients == null) {
			if (other.recipients != null)
				return false;
		} else if (!recipients.equals(other.recipients))
			return false;
		if (senders == null) {
			if (other.senders != null)
				return false;
		} else if (!senders.equals(other.senders))
			return false;
		if (subject == null) {
			if (other.subject != null)
				return false;
		} else if (!subject.equals(other.subject))
			return false;
		return true;
	}

	public void setSendDate(Date date) {
		this.sendDate=date;
		
	}

	public Date getSendDate() {
		return sendDate;
	}


	


}
