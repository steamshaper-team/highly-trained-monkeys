package org.steamshaper.htm.jpa.entities.mail;

import java.io.File;
import java.io.IOException;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Transient;

import org.apache.commons.codec.digest.DigestUtils;
import org.apache.commons.io.FileUtils;
import org.steamshaper.htm.H;

@Entity
public class EMailAttachments {
	
	private static final String ATTACHMENT_EXT = ".attachment";

	private String mimeType;
	private String name;
	private String filePath;
	
	private String sha1 = "";
	
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Long id;
	

	@Transient
	private byte[] content;
	
	public String getMimeType() {
		return mimeType;
	}

	public void setMimeType(String mimeType) {
		this.mimeType = mimeType;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public byte[] getContent(){
		if(content==null && id!=null){
			try {
				content =  FileUtils.readFileToByteArray(new File(this.filePath));
			} catch (IOException e) {
				e.getStackTrace();
			}
		}
		return content;
	}

	public void setContent(byte[] content)  {
		
		if(content==null){
			
		}else{
		this.content = content;
		try {
			saveAttachmentToFile();
		} catch (IOException e) {
			e.printStackTrace();
		}
		}
	}

	
	private void saveAttachmentToFile() throws IOException{
		this.sha1=DigestUtils.sha1Hex(content);
		File file = H.fsTools.createResource(getSha1()+ATTACHMENT_EXT,"data","attachments");
		FileUtils.writeByteArrayToFile(file, content);
		this.filePath=file.getAbsolutePath();
	}


	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((filePath == null) ? 0 : filePath.hashCode());
		result = prime * result
				+ ((mimeType == null) ? 0 : mimeType.hashCode());
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		return result;
	}


	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		EMailAttachments other = (EMailAttachments) obj;
		if (filePath == null) {
			if (other.filePath != null)
				return false;
		} else if (!filePath.equals(other.filePath))
			return false;
		if (mimeType == null) {
			if (other.mimeType != null)
				return false;
		} else if (!mimeType.equals(other.mimeType))
			return false;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		return true;
	}


	public EMailAttachments(){
		
	}


	public EMailAttachments(String name, byte[] content,String mimeType) {
		this.name =  name;
		this.setContent(content);
		this.mimeType = mimeType;
	}


	public String getFilePath() {
		return filePath;
	}

	@Override
	public String toString() {
		return "MailAttachments [mimeType=" + mimeType + ", name=" + name + "]";
	}

	public String getSha1() {
		return sha1;
	}
	
}
