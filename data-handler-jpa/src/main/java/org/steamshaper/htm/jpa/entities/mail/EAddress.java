package org.steamshaper.htm.jpa.entities.mail;

import javax.persistence.Entity;
import javax.persistence.Id;


@Entity
public class EAddress {
	
	
	@Id
	private String mail;


	public String getMail() {
		return mail;
	}


	public void setMail(String mail) {
		this.mail = mail.toLowerCase();
	}


	@Override
	public String toString() {
		return "Address [mail=" + mail + "]";
	}


	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((mail == null) ? 0 : mail.hashCode());
		return result;
	}


	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		EAddress other = (EAddress) obj;
		if (mail == null) {
			if (other.mail != null)
				return false;
		} else if (!mail.equals(other.mail))
			return false;
		return true;
	}

}
