package org.steamshaper.htm.jpa.repos;

import org.springframework.data.jpa.repository.JpaRepository;
import org.steamshaper.htm.jpa.entities.issuemanagement.EIssue;

public interface EIssueRepo extends JpaRepository<EIssue, Long> {

}
