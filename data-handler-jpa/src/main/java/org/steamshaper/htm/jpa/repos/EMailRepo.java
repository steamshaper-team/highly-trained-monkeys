package org.steamshaper.htm.jpa.repos;

import java.util.List;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.steamshaper.htm.jpa.entities.mail.EEMail;

public interface EMailRepo extends JpaRepository<EEMail, Long> {

	List<EEMail> findAllByProcessingTimestampIs(Long timestamp);
	
	@Query("select e from EEMail e where e.processingTimestamp = ?1")
	List<EEMail> findByProcessingTimestampIs(Long timestamp,Pageable page);
}
