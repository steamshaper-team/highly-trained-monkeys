package org.steamshaper.htm.jpa.repos;

import org.springframework.data.jpa.repository.JpaRepository;
import org.steamshaper.htm.jpa.entities.issuemanagement.EUser;

public interface EUserRepo extends JpaRepository<EUser, String> {

}
