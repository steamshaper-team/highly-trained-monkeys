package org.steamshaper.htm.jpa.entities.issuemanagement;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinTable;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;

/**
 * Redmine's Issue
 */
@Entity
public class EIssue {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	@Column(length=4096)
	private String subject;

	@OneToOne
	private EUser assignee;

	@OneToOne
	private EIssuePriority priority;

	@OneToOne
	private EProject project;

	@OneToOne
	private EUser author;

	@OneToOne
	private ETracker tracker;
	
	@Column(length=65536)
	private String description;

	@OneToOne
	private EIssueStatus status;


	@OneToMany(cascade=CascadeType.ALL)
	@JoinTable(name = "issue_to_attachments")
	private List<EAttachment> attachments = new ArrayList<EAttachment>();
	
	@OneToMany
	@JoinTable(name = "issue_to_watchers")
	private List<EUser> watchers = new ArrayList<EUser>();

	public String getSubject() {
		return subject;
	}

	public void setSubject(String subject) {
		this.subject = subject;
	}

	public EUser getAssignee() {
		return assignee;
	}

	public void setAssignee(EUser assignee) {
		this.assignee = assignee;
	}

	public EIssuePriority getPriority() {
		return priority;
	}

	public void setPriority(EIssuePriority priority) {
		this.priority = priority;
	}

	public EProject getProject() {
		return project;
	}

	public void setProject(EProject project) {
		this.project = project;
	}

	public EUser getAuthor() {
		return author;
	}

	public void setAuthor(EUser author) {
		this.author = author;
	}

	public ETracker getTracker() {
		return tracker;
	}

	public void setTracker(ETracker tracker) {
		this.tracker = tracker;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public EIssueStatus getStatus() {
		return status;
	}

	public void setStatus(EIssueStatus status) {
		this.status = status;
	}

	public List<EAttachment> getAttachments() {
		return attachments;
	}

	public void setAttachments(List<EAttachment> attachments) {
		this.attachments = attachments;
	}

	public List<EUser> getWatchers() {
		return watchers;
	}

	public void setWatchers(List<EUser> watchers) {
		this.watchers = watchers;
	}

	public Long getId() {
		return id;
	}

	@Override
	public String toString() {
		return "Issue [id=" + id + ", subject=" + subject + "]";
	}

}
