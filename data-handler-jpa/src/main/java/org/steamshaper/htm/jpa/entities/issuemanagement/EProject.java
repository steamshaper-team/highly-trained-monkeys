package org.steamshaper.htm.jpa.entities.issuemanagement;

import java.util.Date;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;

/**
 * Redmine's Project.
 */
@Entity
public class EProject {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String name;

    private String description;

    private String homepage;

    private Date createdOn;

    private Date updatedOn;

    /**
     * Trackers available for this project
     */
    @ManyToMany
    @JoinTable(name = "projects_to_tracker")
    private List<ETracker> trackers;

    /**
     * This is the *database ID*, not a String-based key.
     */
    private Integer parentId;


    public String getHomepage() {
        return homepage;
    }

    public void setHomepage(String homepage) {
        this.homepage = homepage;
    }

    /**
     * @return numeric database ID
     */
    public Long getId() {
        return id;
    }

    /**
     * @param id numeric database ID
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * @return project name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name the project name
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return list of Trackers allowed in this project (e.g.: Bug, Feature, Support, Task, ...)
     */
    public List<ETracker> getTrackers() {
        return trackers;
    }

    public void setTrackers(List<ETracker> trackers) {
        this.trackers = trackers;
    }

    public ETracker getTrackerByName(String trackerName) {
        if (this.trackers == null) return null;
        for (ETracker t : this.trackers) {
            if (t.getName().equals(trackerName)) return t;
        }
        return null;
    }

    @Override
    public String toString() {
        return name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Date getCreatedOn() {
        return createdOn;
    }

    public void setCreatedOn(Date createdOn) {
        this.createdOn = createdOn;
    }

    public Date getUpdatedOn() {
        return updatedOn;
    }

    public void setUpdatedOn(Date updatedOn) {
        this.updatedOn = updatedOn;
    }

    /**
     * Redmine's REST API "get project" operation does NOT return the 
     * parent project ID in redmine 1.1.2 (and maybe earlier). Which means 
     * calling getParentId() of the project loaded from Redmine server will
     * return <b>NULL</b> with that redmine. This bug was fixed in redmine 1.2.1.
     * See bug http://www.redmine.org/issues/8229
     * 
     *
     * @return the parent project Id if it was set programmatically or NULL (!!!) if the project was loaded from the server.
     */
    public Integer getParentId() {
        return parentId;
    }

    public void setParentId(Integer parentId) {
        this.parentId = parentId;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        EProject project = (EProject) o;

        if (id != null ? !id.equals(project.id) : project.id != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        return id != null ? id.hashCode() : 0;
    }
}
