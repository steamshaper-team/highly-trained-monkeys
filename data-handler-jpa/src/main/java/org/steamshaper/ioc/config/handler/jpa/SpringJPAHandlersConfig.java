package org.steamshaper.ioc.config.handler.jpa;

import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.ImportResource;

@Configuration
@ImportResource("classpath:/META-INF/spring/jpa-handler-conf.xml")
public class SpringJPAHandlersConfig {

}
