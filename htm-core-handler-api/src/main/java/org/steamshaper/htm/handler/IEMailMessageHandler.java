package org.steamshaper.htm.handler;

import java.util.List;

import org.steamshaper.htm.beans.emails.EMail;

public interface IEMailMessageHandler {

	void registerEmail(EMail message);
	
	List<EMail> getEMailToBeProcessed();

	List<EMail> getEMailToBeProcessed(int maxResults);
	
	EMail getFirstMailToBeProcessed();

	void markAsProcessed(EMail email);
	
	

}
