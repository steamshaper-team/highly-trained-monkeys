package org.steamshaper.htm.handler;


import java.util.List;

import org.steamshaper.htm.beans.redmine.RedmineIssueMapper;

import com.taskadapter.redmineapi.RedmineManager.INCLUDE;
import com.taskadapter.redmineapi.bean.Attachment;
import com.taskadapter.redmineapi.bean.Issue;
import com.taskadapter.redmineapi.bean.IssuePriority;
import com.taskadapter.redmineapi.bean.Tracker;
import com.taskadapter.redmineapi.bean.User;


public interface IRedmineHandler {

	RedmineIssueMapper findMappingByMarker(String extTicketID);
	
	void saveMapping(RedmineIssueMapper mapper);

	User getDefaultUser();

	User getUserById(int userId);

	List<Tracker> getTrackers();

	List<IssuePriority> getAllIssuesPriority();

	Issue findIssueById(Integer redmineIssueID);

	Issue findIssueById(Integer redmineIssueID, INCLUDE... include);

	void updateIssue(Issue existingIssue);

	Issue createIssue(String string, Issue newIssue);

	Attachment uploadAttachment(String partName, String mime, byte[] payload,
			int length);


	
	List<User> getAllUsers();
	
	
	
	
	

}
