package org.steamshaper.htm.handler;

import java.util.List;

import org.steamshaper.htm.beans.ManagedService;
import org.steamshaper.htm.beans.ServiceDescriptor.STATUS;
import org.steamshaper.htm.servicesManager.services.IHTMService;

public interface IServiceManagementHandler {
	
	
	List<ManagedService> getAllRegisteredServices();

	IHTMService findServiceByName(String targetServiceId);
	
	boolean changeStatus(IHTMService target, STATUS newStatus);
	

}
