package org.steamshaper.htm.handler.exception;


public class SteamshaperHandlerRuntimeException extends RuntimeException {

	public SteamshaperHandlerRuntimeException(String errorMessage, Exception e) {
		super(errorMessage,e);
	}

	public SteamshaperHandlerRuntimeException(String string) {
		super(string);
	}

	/**
	 * 
	 */
	private static final long serialVersionUID = -1231408042910589391L;

}
