package org.steamshaper.htm.handler;

import org.steamshaper.htm.beans.issuemanagement.IssueBean;
import org.steamshaper.htm.beans.issuemanagement.UserBean;

public interface IIssuesHandler {

	IssueBean saveIssue(IssueBean intIssue);

	IssueBean findIssueById(long internalIssue);
	
	UserBean findUserByLoginName(String login);

	UserBean addUser(UserBean user);
	
	boolean existUser(String login);

	
	
}
