package org.steamshaper.htm.servicesManager.services;

import org.steamshaper.htm.beans.ExecutionResults;
import org.steamshaper.htm.beans.ServiceDescriptor;


public interface IHTMService {

	ExecutionResults execute();

	ServiceDescriptor getServiceDescription();
	
	void init();
	
}
