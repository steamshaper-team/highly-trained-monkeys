package org.steamshaper.htm.servicesManager.services;

import org.steamshaper.htm.servicesManager.clocks.IServiceClock;


public interface IHTMScheduledService extends IHTMService {

	IServiceClock getServiceClock();
}
