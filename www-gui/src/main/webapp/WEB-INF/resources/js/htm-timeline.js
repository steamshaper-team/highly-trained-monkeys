/**
 * 
 */
var omnitimelineTemplate = {
	templates : {
		controls : _
				.template('<div class="row"><div class="col-md-offset-10 col-md-2 text-right "><div class="btn btn-primary" id="timeline-refresh-btn"><span class="glyphicon glyphicon-refresh"></span></div>&nbsp;<div type="button" class="btn btn-primary" id="timeline-sound-btn"><span class="glyphicon glyphicon-volume-up"></span></div></div></div>'),
		timelineContainer : _
				.template('<ul class="timeline" id="tweet-timeline"></ul>'),
		timelineEntry : _
				.template('<li <%= alternate %>><div class="timeline-badge <%= severity %>"><i class="fa <%= fontAwesomeGlyph %>"></i></div><div class="timeline-panel"><div class="timeline-heading"><h4 class="timeline-title"><%= title %></h4><p><small class="text-muted"><%= timeDescription %> via <%= source %></small></p></div><div class="timeline-body"><p><%= body %></p></div></div></li>')
	}
}

var timelineRendered = function(idx, value, entries) {
	var alternateClass = (idx % 2 == 0) ? '' : 'class="timeline-inverted"'

	entries.push(omnitimelineTemplate['templates'].timelineEntry({
		idx : idx,
		alternate : alternateClass,
		severity : value.severity,
		fontAwesomeGlyph : value.fontAwesomeGlyph,
		title : value.title,
		timeDescription : value.timeDescription,
		source : value.source,
		body : value.body
	}));
}

function createTimeline() {
	var omnilineContainer = $('.omniline');
	if (omnilineContainer) {
		omnilineContainer.append(omnitimelineTemplate['templates'].controls());
		omnilineContainer.append(omnitimelineTemplate['templates']
				.timelineContainer());
		$(
		'<audio id="notification-sounds"><source src="raw/sounds/notify.ogg" type="audio/ogg"/><source src="raw/sounds/notify.mp3" type="audio/mpeg"/><source src="raw/sounds/notify.wav" type="audio/wav"/></audio>')
		.appendTo('body');
		startSound('#notification-sounds');
		loadTimeline('tweets', 'tweet-timeline', timelineRendered);
		startRefresh();
		controlButton();
	}

}

var eventsRefreshTimer;

function startRefresh() {
	if (!eventsRefreshTimer) {
		alertify.log("Message auto refresh ENABLED!");
		eventsRefreshTimer = setInterval(function() {
			loadTimeline('tweets', 'tweet-timeline', timelineRendered,
					"#notification-sounds");
		}, 15000);
	}
}

function stopRefresh() {
	clearInterval(eventsRefreshTimer);
	alertify.log("Message auto refresh DISABLED!");
	eventsRefreshTimer = false;
}

function controlButton() {
	var refreshBtn = $('#timeline-refresh-btn');
	refreshBtn.click(function() {
		if (!eventsRefreshTimer) {
			refreshBtn.removeClass("btn-default").addClass("btn-primary");
			startRefresh();

		} else {
			refreshBtn.removeClass("btn-primary").addClass("btn-default");
			stopRefresh();
		}

	});

	var soundButton = $('#timeline-sound-btn');
	soundButton.click(function() {
		if (!soundElement) {
			soundButton.removeClass("btn-default").addClass("btn-primary");
			soundButton.children().removeClass("glyphicon-volume-off").addClass("glyphicon-volume-up");
			
			startSound();

		} else {
			soundButton.removeClass("btn-primary").addClass("btn-default");
			soundButton.children().removeClass("glyphicon-volume-up").addClass("glyphicon-volume-off");
			stopSound();
		}

	});
}

function startSound(startSoundID) {

	if(startSoundID){
		soundElementID = startSoundID;
	}
	soundElement = $(soundElementID);
	alertify.log('<span class="glyphicon glyphicon-volume-up"></span>');
}
function stopSound() {
	soundElement = false;
	alertify.log('<span class="glyphicon glyphicon-volume-off"></span>');
}

var soundElement, soundElementID;

function loadTimeline(timelineId, containerElementID, entryRenderer) {
	alertify
	.success('<i class="fa fa-refresh fa-fw fa-spin"></i>');
	$
			.ajax({
				url : 'json/' + timelineId + '/timeline',
				data : 'fromTimestamp=10',
				dataType : 'json',
				success : function(data) {
					target = $('#' + containerElementID);
					target.html('');

					entries = [];

					$.each(data.eventsList, function(index, value) {
						entryRenderer(index, value, entries)
					});

					// entries.reverse();
					var outputHtml = '';
					$.each(entries, function(index, value) {
						outputHtml += value;
					});
					target.hide();
					target.append(outputHtml);
					target.fadeIn(600);

					if ( soundElement != null && soundElement!=false && soundElement[0]!=null){
						soundElement[0].play();
					}
				},
				error : function(request, status, error) {
					alertify
							.error('<span class="badge badge-white">Connection lost!</span> <b>Unable to fetch messages</b>');
				}

			});
}
