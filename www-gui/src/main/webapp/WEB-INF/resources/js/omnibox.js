/**
 * 
 */

$('#omnibox')
		.atwho(
				{
					at : "@",
					display_timeout : 3000,
					tpl : "<li data-value='@${name}'>${name} <small>${mail}</small></li>",
					insert_tpl : "<span id='${id}' class='badge badge-info'>${atwho-data-value}</span>",
					data : null,
					show_the_at : true,
					callbacks : {
						/*
						 * It function is given, At.js will invoke it if local
						 * filter can not find any data @param query [String]
						 * matched query @param callback [Function] callback to
						 * render page.
						 */
						remote_filter : function(query, callback) {
							$.getJSON("json/users/query", {
								q : query
							}, function(data) {
								console.log(data);
								callback($.map(data, function(value, i) {
									return {
										id : value.id,
										name : value.name,
										mail : value.mail
									}
								}));
							});
						}
					}
				});

$('#omnibox').keyup(
		function(event) {
			if (event.ctrlKey && (event.which == 10 || event.which == 13)) {
				sendMessage();
				alertify.log('<b>Ctrl+Enter pressed ! TODO: ajax call!</b>'
						+ event.which);
			}
			return event;

		});

$('#omnibox-button').click(
		function(event) {
			event.preventDefault();
			sendMessage()
		});

function sendMessage(){
	var omniboxText =$("#omnibox");
	var msgBody = omniboxText.text();
	omniboxText.text("");
	var msgData =  {
			  'msg': msgBody
	  }
	 // msgData =$.parseJSON(msgData);
	$.ajax({
		  type: "POST",
		  url: 'omnibox/send',
		  data: JSON.stringify(msgData),
		  dataType: 'json',
		  success: function (results){
			  loadTimeline('tweets', 'tweet-timeline', timelineRendered,
				"#notification-sounds");
			  },
		});
}
