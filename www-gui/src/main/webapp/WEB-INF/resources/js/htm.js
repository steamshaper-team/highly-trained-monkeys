/**
 * 
 */

function openExternalURLInModalLG(url, title) {
	$('#lg-modal').modal('show');
	iframe = document.getElementById('lg-modal-iframe');

	if (!iframe) {
		iframe = document.createElement('iframe');
		iframe.id = 'lg-modal-iframe';
		iframe.frameBorder="0";
		iframe.className ="modal-iframe"
		lgBody = document.getElementById('lg-modal-body');
		lgBody.innerHTML = '';
		lgBody.appendChild(iframe);
		 $(document).ready(function () {
		        $('#iframe1').on('load', function () {
		            $('#lg-loader').hide();
		        });
		    });

	}
	$('#lg-modal-label').text(title);
	$('#lg-modal-label-url').text(url);
	iframe.height = (lgBody.style.maxHeight.replace('px', '') - 40) + 'px'
	iframe.src = url;
	iframe.style.display = 'block';

}

$("#lg-modal").on("show.bs.modal", function() {
	var maxHeight = $(window).height() - 120;
	$(this).find(".modal-body").css("max-height", maxHeight);
});
$("#lg-modal").on("hidden.bs.modal", function() {
	lgBody = document.getElementById('lg-modal-body');
	lgBody.innerHTML = '';
});


function htmlEntities(str) {
    return String(str).replace(/&/g, '&amp;').replace(/</g, '&lt;').replace(/>/g, '&gt;').replace(/"/g, '&quot;');
}

