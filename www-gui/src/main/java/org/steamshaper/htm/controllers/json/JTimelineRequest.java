package org.steamshaper.htm.controllers.json;

public class JTimelineRequest {
	
	private long fromTimestamp =  0;

	public long getFromTimestamp() {
		return fromTimestamp;
	}

	public void setFromTimestamp(long fromTimestamp) {
		this.fromTimestamp = fromTimestamp;
	}
	

}
