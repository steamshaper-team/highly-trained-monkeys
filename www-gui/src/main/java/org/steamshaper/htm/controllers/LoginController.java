package org.steamshaper.htm.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class LoginController extends RootUIController {

	
	@RequestMapping("/login")
	public String loginForm(){
		return "login/welcome";
	}
}
