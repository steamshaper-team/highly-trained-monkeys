package org.steamshaper.htm.controllers;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.steamshaper.htm.Help;
import org.steamshaper.htm.diagnostic.IProbe;
import org.steamshaper.htm.events.IEventsManager;
import org.steamshaper.htm.events.bean.Event;

import com.fasterxml.jackson.databind.ObjectMapper;

@Controller
@RequestMapping("/omnibox")
public class OmniboxController extends RootController {

	private static final IProbe LOG = Help.withDiagnostic
			.getProbe("www.ctrl.omnibox");
	
	private ObjectMapper jsonMapper = new ObjectMapper();
	
	@Autowired
	protected IEventsManager mainTimeLineManager;
	
	@SuppressWarnings("unchecked")
	@RequestMapping(value="send",method = RequestMethod.POST)
	public @ResponseBody Map<String,String> sendMessage(HttpServletRequest request){
		HashMap<String,String> input = null;
		try {
			input =(HashMap<String,String>) jsonMapper.readValue(request.getInputStream(), HashMap.class);
			Event event = new Event();
			event.setBody(input.get("msg"));
			event.setTitle("Omnibox Message");
			event.setFontAwesomeGlyph("fa-envelope-square");
			event.setSource("CRM-Mail");
			event.setSeverity("success");
			mainTimeLineManager.pushEvent(event);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		LOG.error("Call received");
		
		return input;
		
	}
	
	
}
