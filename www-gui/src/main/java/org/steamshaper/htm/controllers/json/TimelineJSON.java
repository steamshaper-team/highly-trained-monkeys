package org.steamshaper.htm.controllers.json;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.ocpsoft.prettytime.PrettyTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.steamshaper.htm.controllers.RootController;
import org.steamshaper.htm.controllers.session.bean.ui.timelines.TimelineEvent;
import org.steamshaper.htm.events.IEventsManager;
import org.steamshaper.htm.events.bean.Event;

@Controller
@RequestMapping("/json")
public class TimelineJSON extends RootController {

	@Autowired
	protected IEventsManager mainTimeLineManager;

	@RequestMapping(value = "{id}/timeline", method = RequestMethod.GET)
	public @ResponseBody JTimelineResponse getTimelineEntry(
			@PathVariable String id, @RequestParam Long fromTimestamp) {
		List<TimelineEvent> timelineEvents = new ArrayList<TimelineEvent>(
				mainTimeLineManager.getEventCount());
		PrettyTime pt = new PrettyTime(new Date());
		TimelineEvent tlEvent = null;
		for (Event event : mainTimeLineManager.getAllEvents()) {
			tlEvent = mapper.map(event, TimelineEvent.class);
			tlEvent.setTimeDescription(pt.format(event.getInsertDate()));
			timelineEvents.add(tlEvent);
		}
		JTimelineResponse response = new JTimelineResponse();
		response.setEventsList(timelineEvents);
		return response;
	}

	@RequestMapping(value = "users/query", method = RequestMethod.GET)
	public @ResponseBody List<JBUser> queryUsers(@RequestParam(value="q") String query) {
		List<JBUser> output = new ArrayList<JBUser>();

		JBUser user = createUser("1", "Test", "danielefiungo@gmail.com");

		output.add(user);

		user = createUser("2", "Bimbo Gigi", "bimbogigi@iad2.it");
		
		output.add(user);

		return output;
	}

	private JBUser createUser(String id, String name, String mail) {
		JBUser user = new JBUser();
		user.setId(id);
		user.setName(name);
		user.setMail(mail);
		return user;
	}

}
