package org.steamshaper.htm.controllers.json;

import java.io.Serializable;
import java.util.List;

import org.steamshaper.htm.controllers.session.bean.ui.timelines.TimelineEvent;

public class JTimelineResponse  implements Serializable{

	
	public List<TimelineEvent> getEventsList() {
		return eventsList;
	}

	/**
	 * 
	 */
	private static final long serialVersionUID = -4682620277373520845L;

	public JTimelineResponse(){
		
	}
	
	private List<TimelineEvent> eventsList;

	public void setEventsList(List<TimelineEvent> timelineEvents) {
		this.eventsList = timelineEvents;

	}

}
