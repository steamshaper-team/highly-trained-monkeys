package org.steamshaper.htm.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.steamshaper.htm.diagnostic.IDiagnosticService;
import org.steamshaper.htm.diagnostic.IListableProbe;
import org.steamshaper.htm.monkeyservice.IServiceManagement;
import org.steamshaper.htm.monkeyservice.beans.ManagedServiceBean;
import org.steamshaper.htm.service.interfaces.ServiceRequest;
import org.steamshaper.htm.service.interfaces.ServiceResponse;

@Controller
@RequestMapping("/settings")
public class SettingsController extends RootUIController {
	
	@Autowired
	private IServiceManagement srvManager;
	
	
	@Autowired
	IDiagnosticService diagnostic;
	
	@RequestMapping("welcome")
	public String welcome(Model model){
		
		
		ServiceResponse<List<IListableProbe>> diagnosticResponse = diagnostic.getAllProbe(new ServiceRequest<Void>(null));
		List<IListableProbe> probes = diagnosticResponse.getPayload();
		
		model.addAttribute("probeList", probes);
		
		
		
		return "settings/welcome";
	}

	
	
	@ModelAttribute("allServices")
	public List<ManagedServiceBean>  getAllRegisteredServices(){
		List<ManagedServiceBean> output = srvManager.getAllManagedService().getPayload();
		return output;
	}
}
