package org.steamshaper.htm.controllers;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.steamshaper.htm.beans.ServiceDescriptor;
import org.steamshaper.htm.monkeyservice.IServiceManagement;
import org.steamshaper.htm.monkeyservice.beans.ExecuteActionOnServiceRequest;
import org.steamshaper.htm.monkeyservice.beans.ExecuteActionOnServiceResponse;
import org.steamshaper.htm.service.interfaces.ServiceRequest;
import org.steamshaper.htm.service.interfaces.ServiceResponse;

@Controller
public class CommandController extends RootController {

	@Autowired
	private IServiceManagement srvManager;

	@RequestMapping(value = "/srvmod",method=RequestMethod.POST)
	public String srvMod(@RequestParam("service-id") String serviceId, @RequestParam("service-action") ServiceDescriptor.STATUS targetStatus, @RequestParam("back-to") String backTo,HttpServletRequest httpRequest) {
		
		ExecuteActionOnServiceRequest reqPayload =  new ExecuteActionOnServiceRequest();
		
		reqPayload.setTargetServiceId(serviceId);
		reqPayload.setTargetStatus(targetStatus);
		
		ServiceRequest<ExecuteActionOnServiceRequest> request =  new ServiceRequest<ExecuteActionOnServiceRequest>(reqPayload);
		
		ServiceResponse<ExecuteActionOnServiceResponse> response = srvManager.manage(request);
		backTo = removeContextPath(backTo, httpRequest);
		return "redirect:"+backTo;
	}

	private String removeContextPath(String backTo,
			HttpServletRequest httpRequest) {
		if(backTo.startsWith(httpRequest.getContextPath())){
			backTo = backTo.replaceFirst(httpRequest.getContextPath(),"");
		}
		return backTo;
	}
}
