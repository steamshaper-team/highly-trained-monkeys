package org.steamshaper.htm.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.steamshaper.htm.controllers.session.bean.AppInfos;
import org.steamshaper.htm.controllers.session.bean.ui.dashboard.DashBoardBean;


public abstract class RootUIController extends RootController {

	

	@Autowired
	protected DashBoardBean mainDashboard;
	

	

	


	@ModelAttribute(value = "appInfos")
	protected AppInfos getAppInfos() {
		return this.appInfos;
	}
	
	@ModelAttribute(value = "mainDashboard")
	protected DashBoardBean getMainDashboard() {
		return this.mainDashboard;
	}
	

}
