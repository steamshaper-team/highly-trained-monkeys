package org.steamshaper.htm.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.steamshaper.htm.Help;
import org.steamshaper.htm.diagnostic.IProbe;

@Controller
public class HomePageController extends RootUIController {

	private static final IProbe LOG = Help.withDiagnostic
			.getProbe("www.ctrl.home");
	
	@RequestMapping("/")
	public String home() {
		return "home/welcome";
	}
	


	
//	@ModelAttribute(value="mainTimeline")
//	protected List<TimelineEvent> getMainTimelineEvents(){
//		List<TimelineEvent> timelineEvents =  new ArrayList<TimelineEvent>(mainTimeLineManager.getEventCount());
//		PrettyTime pt =  new PrettyTime(new Date());
//		TimelineEvent tlEvent = null;
//		for(Event event : mainTimeLineManager.getAllEvents()){
//			tlEvent = mapper.map(event, TimelineEvent.class);
//			tlEvent.setTimeDescription(pt.format(event.getInsertDate()));
//			timelineEvents.add(tlEvent);
//		}
//		return timelineEvents;
//	}
}
