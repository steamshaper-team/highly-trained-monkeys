package org.steamshaper.htm.controllers.session.bean.ui.dashboard;

public class DashBoardElement {

	private String imgPath;

	private String title;

	private String description;

	private boolean modal =  false;

	private boolean directLink = false;

	private String target = "#";

	public String getImgPath() {
		return imgPath;
	}

	public void setImgPath(String imgPath) {
		this.imgPath = imgPath;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public boolean isModal() {
		return modal;
	}

	public void setModal(boolean modal) {
		this.modal = modal;
	}

	public boolean isDirectLink() {
		return directLink;
	}

	public void setDirectLink(boolean directLink) {
		this.directLink = directLink;
	}

	public String getTarget() {
		return target;
	}

	public void setTarget(String target) {
		this.target = target;
	}
}
