package org.steamshaper.htm.controllers.session.bean.ui.dashboard;

import java.util.ArrayList;
import java.util.List;

public class DashBoardBean {
	
	private String title;

	private List<DashBoardElement> elements;

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public List<DashBoardElement> getElements() {
		if(elements==null){
			elements = new ArrayList<>();
		}
		return elements;
	}

	public void setElements(List<DashBoardElement> elements) {
		this.elements = elements;
	}

}
