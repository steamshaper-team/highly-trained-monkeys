package org.steamshaper.htm.controllers.session.bean.ui.dashboard;


public class DashboardFactory {
	
	private static DashBoardBean singleton = null;
	
	public DashBoardBean getDashboard(){
		if(singleton==null){
			singleton = new DashBoardBean();
			
			//Redmine
			DashBoardElement redElement = new DashBoardElement();
			redElement.setTitle("Redmine");
			redElement.setDescription("Issue Management");
			redElement.setDirectLink(true);
			redElement.setModal(true);
			redElement.setTarget("http://www.hpfactory.it/redmine/");
			redElement.setImgPath("/assets/apps/redmine_xs.png");
			
			singleton.getElements().add(redElement);
			
			//Jenkins
			DashBoardElement jenkinsElement = new DashBoardElement();
			jenkinsElement.setTitle("Jenkins");
			jenkinsElement.setDescription("Continuos Integration");
			jenkinsElement.setDirectLink(true);
			jenkinsElement.setModal(true);
			jenkinsElement.setTarget("http://www.hpfactory.it/jenkins/");
			jenkinsElement.setImgPath("/assets/apps/jenkins_xs.png");
			
			singleton.getElements().add(jenkinsElement);
		
			//Git
			DashBoardElement gitElement = new DashBoardElement();
			gitElement.setTitle("Git");
			gitElement.setDescription("Source Code Management");
			gitElement.setDirectLink(true);
			gitElement.setModal(false);
			gitElement.setTarget("http://gitscm.hpfactory.it/");
			gitElement.setImgPath("/assets/apps/git_xs.png");
			
			singleton.getElements().add(gitElement);
			
			//Git
			DashBoardElement nexusElement = new DashBoardElement();
			nexusElement.setTitle("Nexus");
			nexusElement.setDescription("Maven Artifact Repository ");
			nexusElement.setDirectLink(true);
			nexusElement.setModal(false);
			nexusElement.setTarget("http://www.hpfactory.it/nexus/");
			nexusElement.setImgPath("/assets/apps/nexus_xs.png");
			
			singleton.getElements().add(nexusElement);
		}
		return singleton;
	}



}
