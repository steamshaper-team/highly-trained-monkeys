package org.steamshaper.htm.controllers.session.bean;

public class AppInfos {

	private String applicationName = "";
	private String applicationNameShort = "";
	

	public String getApplicationName() {
		return applicationName;
	}

	public void setApplicationName(String applicationName) {
		this.applicationName = applicationName;
	}

	public String getApplicationNameShort() {
		return applicationNameShort;
	}

	public void setApplicationNameShort(String applicationNameShort) {
		this.applicationNameShort = applicationNameShort;
	}

}
