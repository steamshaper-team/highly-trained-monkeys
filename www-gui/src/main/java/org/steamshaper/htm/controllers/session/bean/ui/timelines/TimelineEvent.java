package org.steamshaper.htm.controllers.session.bean.ui.timelines;

import java.util.Date;

public class TimelineEvent {
	private String title;

	private Date insertDate;

	private String source;

	private String body;

	private String fontAwesomeGlyph;
	
	private String timeDescription;

	private String severity;

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public Date getInsertDate() {
		return insertDate;
	}

	public void setInsertDate(Date insertDate) {
		this.insertDate = insertDate;
	}

	public String getSource() {
		return source;
	}

	public void setSource(String source) {
		this.source = source;
	}

	public String getBody() {
		return body;
	}

	public void setBody(String body) {
		this.body = body;
	}

	public String getFontAwesomeGlyph() {
		return fontAwesomeGlyph;
	}

	public void setFontAwesomeGlyph(String fontAwesomeGlyph) {
		this.fontAwesomeGlyph = fontAwesomeGlyph;
	}

	public String getTimeDescription() {
		return timeDescription;
	}

	public void setTimeDescription(String timeDescription) {
		this.timeDescription = timeDescription;
	}

	public String getSeverity() {
		return severity;
	}

	public void setSeverity(String severity) {
		this.severity = severity;
	}
	
}
