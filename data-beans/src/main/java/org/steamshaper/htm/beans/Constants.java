package org.steamshaper.htm.beans;

public abstract class Constants {

	public static final String MESSAGE_BODY_PREFIX = "__message";
	public static final String MESSAGE_BODY_TXT = MESSAGE_BODY_PREFIX + ".txt";
	public static final String MESSAGE_BODY_HTML = MESSAGE_BODY_PREFIX
			+ ".html";
	public static final String MESSAGE_BODY_UNKNOW = MESSAGE_BODY_PREFIX
			+ ".unknown";

	private Constants() {

	}

}
