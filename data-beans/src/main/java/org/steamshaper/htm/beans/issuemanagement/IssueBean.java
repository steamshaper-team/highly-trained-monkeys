package org.steamshaper.htm.beans.issuemanagement;

import java.util.ArrayList;
import java.util.List;

/**
 * Redmine's Issue
 */
public class IssueBean {

	private Long id;
	
	private String subject;

	private UserBean assignee;

	private IssuePriorityBean priority;

	private ProjectBean project;

	private UserBean author;

	private TrackerBean tracker;
	
	private String description;

	private IssueStatusBean status;


	private List<AttachmentBean> attachments = new ArrayList<>();
	
	private List<UserBean> watchers = new ArrayList<UserBean>();

	public String getSubject() {
		return subject;
	}

	public void setSubject(String subject) {
		this.subject = subject;
	}

	public UserBean getAssignee() {
		return assignee;
	}

	public void setAssignee(UserBean assignee) {
		this.assignee = assignee;
	}

	public IssuePriorityBean getPriority() {
		return priority;
	}

	public void setPriority(IssuePriorityBean priority) {
		this.priority = priority;
	}

	public ProjectBean getProject() {
		return project;
	}

	public void setProject(ProjectBean project) {
		this.project = project;
	}

	public UserBean getAuthor() {
		return author;
	}

	public void setAuthor(UserBean author) {
		this.author = author;
	}

	public TrackerBean getTracker() {
		return tracker;
	}

	public void setTracker(TrackerBean tracker) {
		this.tracker = tracker;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public IssueStatusBean getStatus() {
		return status;
	}

	public void setStatus(IssueStatusBean status) {
		this.status = status;
	}

	public List<AttachmentBean> getAttachments() {
		return attachments;
	}

	public void setAttachments(List<AttachmentBean> attachments) {
		this.attachments = attachments;
	}

	public List<UserBean> getWatchers() {
		return watchers;
	}

	public void setWatchers(List<UserBean> watchers) {
		this.watchers = watchers;
	}

	public Long getId() {
		return id;
	}

	@Override
	public String toString() {
		return "Issue [id=" + getId() + ", subject=" + subject + "]";
	}

	public void setId(Long id) {
		this.id = id;
	}

}
