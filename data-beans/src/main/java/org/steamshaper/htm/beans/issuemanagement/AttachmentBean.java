package org.steamshaper.htm.beans.issuemanagement;

import java.util.Date;

/**
 * File Attachment for a Redmine issue
 */
public class AttachmentBean {

	private Long id;
    private String fileName;
    private long fileSize;
    private String contentType;
    private String contentURL;
    private String description;
    private Date createdOn;
	private String token;
	private String sha1="";
	private String localResourcePath=null;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getContentType() {
        return contentType;
    }

    public void setContentType(String contentType) {
        this.contentType = contentType;
    }

    public String getContentURL() {
        return contentURL;
    }

    public void setContentURL(String contentURL) {
        this.contentURL = contentURL;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Date getCreatedOn() {
        return createdOn;
    }

    public void setCreatedOn(Date createdOn) {
        this.createdOn = createdOn;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public long getFileSize() {
        return fileSize;
    }

    public void setFileSize(long fileSize) {
        this.fileSize = fileSize;
    }

	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}

    public String getSha1() {
		return sha1;
	}

	public void setSha1(String sha1) {
		this.sha1 = sha1;
	}

	@Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        AttachmentBean that = (AttachmentBean) o;

        if (id != null ? !id.equals(that.id) : that.id != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        return id != null ? id.hashCode() : 0;
    }

    @Override
    public String toString() {
        return "IssueAttachment{" +
                "id=" + id +
                ", fileName='" + fileName + '\'' +
                ", fileSize=" + fileSize +
                ", contentType='" + contentType + '\'' +
                ", contentURL='" + contentURL + '\'' +
                ", description='" + description + '\'' +
                ", createdOn=" + createdOn +
                ", token=" + token +
                '}';
    }

	public String getLocalResourcePath() {
		return localResourcePath;
	}

	public void setLocalResourcePath(String localResourcePath) {
		this.localResourcePath = localResourcePath;
	}
}
