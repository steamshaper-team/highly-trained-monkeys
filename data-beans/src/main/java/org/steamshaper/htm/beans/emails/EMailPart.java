package org.steamshaper.htm.beans.emails;

import java.util.Arrays;

public class EMailPart {

	private byte[] payload;

	@Override
	public String toString() {
		return "EMailPart ["
				+ (payload != null ? "payload=" + Arrays.toString(payload)
						+ ", " : "")
				+ (mime != null ? "mime=" + mime + ", " : "")
				+ (partName != null ? "partName=" + partName : "") + "]";
	}

	private String mime;

	private String partName;
	
	private String sha1;
	
	private String localFilePath=null;
	
	public EMailPart(String partName, byte[] bytes, String mimeType) {
		setPartName(partName);
		setPayload(bytes);
		setMime(mimeType);
		
	}

	public EMailPart() {
	}

	public byte[] getPayload() {
		return payload;
	}

	public void setPayload(byte[] payload) {
		this.payload = payload;
	}

	public String getMime() {
		return mime;
	}

	public void setMime(String mime) {
		this.mime = mime;
	}

	public String getPartName() {
		return partName;
	}

	public void setPartName(String partName) {
		this.partName = partName;
	}

	public String getSha1() {
		return sha1;
	}

	public void setSha1(String sha1) {
		this.sha1 = sha1;
	}

	public String getLocalFilePath() {
		return localFilePath;
	}

	public void setLocalFilePath(String localFilePath) {
		this.localFilePath = localFilePath;
	}
	
	
}
