package org.steamshaper.htm.beans.redmine;


/**
 * Redmine's Issue
 */
public class RedmineIssueMapper {

	private Long internalIssue;

	private Integer redmineIssueID;

	private String issueMarker;

	public Long getInternalIssue() {
		return internalIssue;
	}

	public void setInternalIssue(Long internalIssue) {
		this.internalIssue = internalIssue;
	}

	public Integer getRedmineIssueID() {
		return redmineIssueID;
	}

	public void setRedmineIssueID(Integer redmineIssueID) {
		this.redmineIssueID = redmineIssueID;
	}

	public String getIssueMarker() {
		return issueMarker;
	}

	public void setIssueMarker(String issueMarker) {
		this.issueMarker = issueMarker;
	}

}
