package org.steamshaper.htm.beans.emails;

public class EMailAddress {

	@Override
	public String toString() {
		return "EMailAddress [address=" + address + "]";
	}

	private String address;

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;

	}

}
