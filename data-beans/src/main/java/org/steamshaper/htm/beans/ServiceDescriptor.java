package org.steamshaper.htm.beans;



public class ServiceDescriptor {

	public enum SERVICE_TYPE {
		RUN_ONCE, SCHEDULED, AUTO
	}
	
	public enum STATUS {
		STARTED, STOPPED
	}
	private SERVICE_TYPE type;
	
	private String name;
	
	private String version;

	private STATUS initialStatus = STATUS.STARTED;
	
	
	public SERVICE_TYPE getType() {
		return type;
	}

	public void setType(SERVICE_TYPE type) {
		this.type = type;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getVersion() {
		return version;
	}

	public void setVersion(String version) {
		this.version = version;
	}

	public STATUS getInitialStatus() {
		return initialStatus;
	}

	public void setInitialStatus(STATUS initialStatus) {
		this.initialStatus = initialStatus;
	}
	
}
