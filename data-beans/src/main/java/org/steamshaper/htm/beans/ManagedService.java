package org.steamshaper.htm.beans;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

public class ManagedService {

	private ServiceDescriptor descriptor;

	private ServiceDescriptor.STATUS actualStatus;

	private Set<ServiceDescriptor.STATUS> availableTargetStatus = new HashSet<>(Arrays
			.asList(ServiceDescriptor.STATUS.values()));


	public ServiceDescriptor getDescriptor() {
		return descriptor;
	}

	public void setDescriptor(ServiceDescriptor descriptor) {
		this.descriptor = descriptor;
	}

	public ServiceDescriptor.STATUS getActualStatus() {
		return actualStatus;
	}

	public void setActualStatus(ServiceDescriptor.STATUS actualStatus) {
		this.actualStatus = actualStatus;
	}

	public Set<ServiceDescriptor.STATUS> getAvailableTargetStatus() {
		return availableTargetStatus;
	}

	public void setAvailableTargetStatus(
			Set<ServiceDescriptor.STATUS> availableTargetStatus) {
		this.availableTargetStatus = availableTargetStatus;
	}
}
