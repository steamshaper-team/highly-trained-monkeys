package org.steamshaper.htm.beans;


public class ExecutionResults {
	
	public enum EXIT_STAUTS{
		OK,ERROR,FATAL
	}
	
	long nextExecution=-1;
	
	private EXIT_STAUTS exitStatus;

	public long getNextExecution() {
		return nextExecution;
	}

	public void setNextExecution(long nextExecution) {
		this.nextExecution = nextExecution;
	}

	public EXIT_STAUTS getExitStatus() {
		return exitStatus;
	}

	public void setExitStatus(EXIT_STAUTS exitStatus) {
		this.exitStatus = exitStatus;
	}

}
