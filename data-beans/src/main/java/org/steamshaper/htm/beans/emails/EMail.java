package org.steamshaper.htm.beans.emails;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Set;

public class EMail {
	
	@Override
	public String toString() {
		return "EMail [senders=" + senders + ", recipients=" + recipients
				+ ", subject=" + subject + ", body=" + body + ", attachments="
				+ attachments + ", sendDate=" + sendDate + ", receivedDate="
				+ receivedDate + ", internalId=" + internalId + "]";
	}

	private Set<EMailAddress> senders;
	
	private Set<EMailAddress> recipients;
	
	private String subject;
	
	private EMailPart body;

	private List<EMailPart> attachments;
	
	
	private Date sendDate;
	
	private Date receivedDate;
	
	private Long internalId;

	public Set<EMailAddress> getSenders() {
		return senders;
	}

	public void setSenders(Set<EMailAddress> sender) {
		this.senders = sender;
	}

	public Set<EMailAddress> getRecipients() {
		return recipients;
	}

	public void setRecipients(Set<EMailAddress> recipients) {
		this.recipients = recipients;
	}

	public String getSubject() {
		return subject;
	}

	public void setSubject(String subject) {
		this.subject = subject;
	}

	public EMailPart getBody() {
		return body;
	}

	public void setBody(EMailPart body) {
		this.body = body;
	}

	public List<EMailPart> getAttachments() {
		if(attachments==null){
			attachments =  new ArrayList<EMailPart>();
		}
		return attachments;
	}

	public void setAttachments(List<EMailPart> attachments) {
		this.attachments = attachments;
	}


	public Date getSendDate() {
		return sendDate;
	}

	public void setSendDate(Date sendDate) {
		this.sendDate = sendDate;
	}

	public Date getReceivedDate() {
		return receivedDate;
	}

	public void setReceivedDate(Date receivedDate) {
		this.receivedDate = receivedDate;
	}

	public Long getInternalId() {
		return internalId;
	}

	public void setInternalId(Long internalId) {
		this.internalId = internalId;
	} 
	
	

}
